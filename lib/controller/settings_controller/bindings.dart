import 'package:get/get.dart';
import 'package:nextrade/controller/settings_controller/controller.dart';

class SettingsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SettingsControllerImp>(() => SettingsControllerImp());
  }
}
