import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/settings_controller/state.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:url_launcher/url_launcher.dart';

abstract class SettingsController extends GetxController {
  initData();
  logOut();
  toLoginPage();
  toAddressPage();
  toOrdersPage();
  contactUs();
  toArchivedOrdersPage();
}

class SettingsControllerImp extends SettingsController {
  final state = SettingsState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.lang = state.myServices.sharedPreferences.getString('lang')!;
  }

  @override
  logOut() {
    state.logOutData.postData(state.apiToken);
    String id = state.myServices.sharedPreferences.getString('id')!;
    FirebaseMessaging.instance.subscribeToTopic('users');
    FirebaseMessaging.instance.subscribeToTopic('users$id');
    state.myServices.sharedPreferences.clear();
    state.myServices.sharedPreferences.setBool('isFirstTime', false);
    state.myServices.sharedPreferences.setString('lang', state.lang);
    toLoginPage();
  }

  @override
  toLoginPage() {
    Get.offAllNamed(AppRoute.login);
  }

  @override
  toAddressPage() {
    Get.toNamed(AppRoute.addressView);
  }

  @override
  toOrdersPage() {
    Get.toNamed(AppRoute.orders);
  }

  @override
  contactUs() async {
    launchUrl(Uri.parse('tel:+963946224688'));
  }

  @override
  toArchivedOrdersPage() {
    Get.toNamed(AppRoute.archivedOrders);
  }
}
