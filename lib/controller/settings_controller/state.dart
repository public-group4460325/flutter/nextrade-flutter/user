import 'package:get/get.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/auth/log_out_data.dart';

class SettingsState {
  MyServices myServices = Get.find();
  late String apiToken;
  late String lang;
  LogOutData logOutData = LogOutData(Get.find());
}
