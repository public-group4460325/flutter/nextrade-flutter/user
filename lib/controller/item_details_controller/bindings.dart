import 'package:get/get.dart';
import 'package:nextrade/controller/item_details_controller/controller.dart';

class ItemDetailsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ItemDetailsControllerImp>(() => ItemDetailsControllerImp());
  }

}