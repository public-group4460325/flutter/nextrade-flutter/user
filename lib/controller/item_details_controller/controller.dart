import 'package:get/get.dart';
import 'package:nextrade/controller/item_details_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';

abstract class ItemDetailsController extends GetxController {
  initData();
  addToCart();
  removeFromCart();
  increaseCount();
  decreaseCount();
}

class ItemDetailsControllerImp extends ItemDetailsController {
  final state = ItemDetailsState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.item = Get.arguments['item'];
    state.count = 1;
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
  }

  @override
  addToCart() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.cartData
          .addToCart(state.item.id!, state.count, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        update();
        toastMessage(message: 'added'.tr);
        update();
        return;
      }
      if (response['msg'] == 'Category Not Found') {
        toastMessage(message: 'category_not_found'.tr);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
      return;
    }
  }

  @override
  removeFromCart() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.cartData
          .removeFromCart(state.item.id!, state.count, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        toastMessage(message: 'added'.tr);
        update();
        return;
      }
      if (response['msg'] == 'Category Not Found') {
        toastMessage(message: 'category_not_found'.tr);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
      return;
    }
  }

  @override
  increaseCount() {
    state.count++;
    update();
  }

  @override
  decreaseCount() {
    if (state.count > 0) {
      state.count--;
      update();
    }
  }
}
