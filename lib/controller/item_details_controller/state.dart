import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/users/cart_data.dart';
import 'package:nextrade/data/models/item_model.dart';


class ItemDetailsState {
  late int count;
  late Item item;
  late StatusRequest statusRequest;
  CartData cartData = CartData(Get.find());
  late String apiToken;
  MyServices myServices = Get.find();
  List subItems = [
    {
      'name': 'Red',
      'id': '1',
      'active': '0',
    },
    {
      'name': 'Black',
      'id': '2',
      'active': '0',
    },
    {
      'name': 'Blue',
      'id': '3',
      'active': '1',
    },
  ];
}
