import 'package:get/get.dart';
import 'package:nextrade/controller/favorites_controller/controller.dart';
import 'package:nextrade/controller/home_controller/controller.dart';
import 'package:nextrade/controller/home_screen_controller/controller.dart';
import 'package:nextrade/controller/settings_controller/controller.dart';

class HomeScreenBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeScreenControllerImp>(() => HomeScreenControllerImp());
    Get.put(SettingsControllerImp());
    Get.put(FavoritesControllerImp());
    Get.put(HomeControllerImp());
  }
}
