import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/view/screens/favorites.dart';
import 'package:nextrade/view/screens/home.dart';
import 'package:nextrade/view/screens/settings.dart';

class HomeScreenState {
  List<Widget> pagesList = [
    const Home(),
    const Favorites(),
    const Settings(),
    const Center(
      child: Text('Page 4'),
    ),
  ];

  List bottomNavBarActions = [
    {
      'title': 'home'.tr,
      'icon': LineIcons.home,
    },
    {
      'title': 'favorites'.tr,
      'icon': LineIcons.heart,
    },
    {
      'title': 'settings'.tr,
      'icon': LineIcons.cog,
    },
    {
      'title': 'profile'.tr,
      'icon': LineIcons.user,
    },
  ];
  int currentPage = 0;
}
