import 'package:get/get.dart';
import 'package:nextrade/controller/home_screen_controller/state.dart';
import 'package:nextrade/core/constants/routes.dart';

abstract class HomeScreenController extends GetxController {
  changePage(int currentPage);
  toCartPage();
}

class HomeScreenControllerImp extends HomeScreenController {
  final state = HomeScreenState();

  @override
  changePage(currentPage) {
    state.currentPage = currentPage;
    update();
  }

  @override
  toCartPage() {
    Get.toNamed(AppRoute.cart);
  }
}
