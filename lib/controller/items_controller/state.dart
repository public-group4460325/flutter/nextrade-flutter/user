import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/users/favorites_data.dart';
import 'package:nextrade/data/data_source/remote/users/home_data.dart';
import 'package:nextrade/data/data_source/remote/users/items_data.dart';

class ItemsState {
  late List categories;
  late int selectedCategory;
  late int categoryId;
  late String apiToken;
  ItemsData itemsData = ItemsData(Get.find());
  FavoritesData favoritesData = FavoritesData(Get.find());
  MyServices myServices = Get.find();
  List items = [];
  List searchItems = [];
  late StatusRequest statusRequest;
  late TextEditingController searchController;
  bool isSearching = false;
  HomeData homeData = HomeData(Get.find());
}
