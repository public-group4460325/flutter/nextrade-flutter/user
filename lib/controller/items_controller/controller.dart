import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/items_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/item_model.dart';

abstract class ItemsController extends GetxController {
  initData();
  changeCategory(int index);
  getItems(int categoryId);
  toItemDetailsPage(Item item);
  addOrRemoveFromFavorites(int itemId);
  searchData();
  onSearch();
  checkSearch(value);
  toNotificationsPage();
}

class ItemsControllerImp extends ItemsController {
  final state = ItemsState();

  @override
  void onInit() {
    initData();
    getItems(state.categoryId);
    super.onInit();
  }

  @override
  initData() {
    state.categories = Get.arguments['categories'];
    state.selectedCategory = Get.arguments['selectedCategory'];
    state.categoryId = state.categories[state.selectedCategory].id;
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.searchController = TextEditingController();
  }

  @override
  changeCategory(index) {
    state.selectedCategory = index;
    state.categoryId = state.categories[state.selectedCategory].id;
    getItems(state.categoryId);
    update();
  }

  @override
  getItems(int categoryId) async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.itemsData.getData(categoryId, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.items =
            response['data'].map<Item>((e) => Item.fromJson(e)).toList();
        update();
        return;
      }
      if (response['msg'] == 'Category Not Found') {
        toastMessage(message: 'category_not_found'.tr);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
      return;
    }
  }

  @override
  toItemDetailsPage(item) {
    Get.toNamed(AppRoute.itemDetails, arguments: {'item': item});
  }

  @override
  addOrRemoveFromFavorites(itemId) async {
    try {
      var response = await state.favoritesData.addOrRemoveFavoritesData(
        itemId,
        state.apiToken,
      );
      if (response['status']) {
        if (state.items.isNotEmpty &&
            state.items.firstWhereOrNull((element) => element.id == itemId) !=
                null) {
          Item item = state.items.firstWhere((element) => element.id == itemId);
          item.favorite = !item.favorite!;
        }
        if (state.searchItems.isNotEmpty) {
          Item searchItem = state.searchItems
              .firstWhereOrNull((element) => element.id == itemId);
          searchItem.favorite = !searchItem.favorite!;
          toastMessage(
            message: searchItem.favorite! ? 'added'.tr : 'removed'.tr,
          );
        }
        update();
        return;
      }
      update();
      return;
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  searchData() async {
    try {
      state.statusRequest = StatusRequest.loading;
      var response = await state.homeData
          .searchData(state.searchController.text, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.searchItems.clear();
        state.searchItems.addAll(response['data'].map((e) => Item.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  onSearch() {
    state.isSearching = true;
    update();
    searchData();
  }

  @override
  checkSearch(value) {
    if (value.isEmpty) {
      state.isSearching = false;
      state.statusRequest = StatusRequest.none;
      update();
    }
  }

  @override
  toNotificationsPage() {
    Get.toNamed(AppRoute.notifications);
  }
}
