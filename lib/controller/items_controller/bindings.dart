import 'package:get/get.dart';
import 'package:nextrade/controller/items_controller/controller.dart';

class ItemsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ItemsControllerImp>(() => ItemsControllerImp());
  }
}
