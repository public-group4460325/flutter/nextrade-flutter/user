import 'package:get/get.dart';
import 'package:nextrade/controller/onboarding_controller/controller.dart';

class OnBoardingBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OnBoardingControllerImp>(() => OnBoardingControllerImp());
  }
}
