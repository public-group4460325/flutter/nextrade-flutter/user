import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/onboarding_controller/state.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/data/data_source/static/static.dart';

abstract class OnBoardingController extends GetxController {
  initData();
  next();
  onPageChanged(int index);
  skip();
}

class OnBoardingControllerImp extends OnBoardingController {
  final state = OnBoardingState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.pageController = PageController();
  }

  @override
  next() {
    state.currentPage++;
    if (state.currentPage >= onBoardingList.length) {
      state.myServices.sharedPreferences.setBool('isFirstTime', false);
      Get.offAllNamed('/login');
    } else {
      state.pageController.animateToPage(
        state.currentPage,
        duration: const Duration(milliseconds: 900),
        curve: Curves.easeInOut,
      );
    }
  }

  @override
  onPageChanged(int index) {
    state.currentPage = index;
    update();
  }

  @override
  skip() {
    state.currentPage = 4;
    state.myServices.sharedPreferences.setBool('isFirstTime', false);
    Get.offAllNamed(AppRoute.login);
  }

  @override
  void dispose() {
    state.pageController.dispose();
    super.dispose();
  }
}
