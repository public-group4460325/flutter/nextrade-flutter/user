import 'package:get/get.dart';
import 'package:nextrade/controller/notifications_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/notification_model.dart';

abstract class NotificationsController extends GetxController {
  initData();
  getData();
  refreshPage();
}

class NotificationsControllerImp extends NotificationsController {
  final state = NotificationsState();

  @override
  void onInit() {
    initData();
    getData();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
  }

  @override
  getData() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.notificationsData.getData(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.data.addAll(response['data']
            .map<Notification>((e) => Notification.fromJson(e))
            .toList());
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
      return;
    }
  }

  @override
  refreshPage() {
    getData();
  }
}
