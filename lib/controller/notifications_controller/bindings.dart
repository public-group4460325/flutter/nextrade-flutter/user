import 'package:get/get.dart';
import 'package:nextrade/controller/notifications_controller/controller.dart';

class NotificationsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NotificationsControllerImp>(() => NotificationsControllerImp());
  }
}
