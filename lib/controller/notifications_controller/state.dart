import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/users/notification_data.dart';

class NotificationsState {
  List data = [];
  StatusRequest statusRequest = StatusRequest.none;
  MyServices myServices = Get.find();
  NotificationsData notificationsData = NotificationsData(Get.find());
  late String apiToken;
}
