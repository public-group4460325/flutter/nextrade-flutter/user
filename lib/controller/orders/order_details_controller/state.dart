import 'dart:async';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/users/orders_data.dart';
import 'package:nextrade/data/models/address_model.dart';
import 'package:nextrade/data/models/order_model.dart';

class OrderDetailsState {
  late Order order;
  late Completer<GoogleMapController> controller;
  List<Marker> markers = [];
  late double lat;
  late double long;
  CameraPosition kGooglePlex =
      const CameraPosition(target: LatLng(0, 0), zoom: 14.4746);
  late String apiToken;
  MyServices myServices = Get.find();
  StatusRequest statusRequest = StatusRequest.none;
  OrdersData ordersData = OrdersData(Get.find());
  Address? address;
  List items = [];
  double totalPrice = 0;
}
