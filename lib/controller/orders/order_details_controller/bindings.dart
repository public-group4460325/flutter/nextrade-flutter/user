import 'package:get/get.dart';
import 'package:nextrade/controller/orders/order_details_controller/controller.dart';

class OrderDetailsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<OrderDetailsControllerImp>(() => OrderDetailsControllerImp());
  }
}
