import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/orders/archived_orders_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/rating_dialog.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/order_model.dart';

abstract class ArchivedOrdersController extends GetxController {
  initData();
  getOrders();
  toOrdersDetailsPage(Order order);
  sendRating(int orderId, double rate, String comment);
  showRating(BuildContext context, int orderId);
}

class ArchivedOrdersControllerImp extends ArchivedOrdersController {
  final state = ArchivedOrdersState();

  @override
  void onInit() {
    initData();
    getOrders();
    super.onInit();
  }

  @override
  void initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    super.onInit();
  }

  @override
  getOrders() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.ordersData.getArchivedOrders(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.orders.addAll(response['data'].map((e) => Order.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toOrdersDetailsPage(order) {
    Get.toNamed(AppRoute.ordersDetails, arguments: {'order': order});
  }

  @override
  sendRating(orderId, rate, comment) async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.ordersData.rateOrder(
        orderId,
        rate,
        comment,
        state.apiToken,
      );
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        toastMessage(message: 'rate.sent'.tr);
        var temp = state.orders.firstWhere((element) => element.id == orderId);
        temp.rated = true;
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  showRating(context, orderId) {
    showRatingDialog(context, orderId);
  }
}
