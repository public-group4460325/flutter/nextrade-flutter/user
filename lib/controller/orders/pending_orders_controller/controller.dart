import 'package:get/get.dart';
import 'package:nextrade/controller/orders/pending_orders_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/order_model.dart';

abstract class PendingOrdersController extends GetxController {
  initData();
  getOrders();
  deleteOrder(int orderId);
  String getOrderStatus(String val);
  String getReceiveType(String val);
  String getPaymentType(String val);
  toOrdersDetailsPage(Order order);
  refreshPage();
}

class PendingOrdersControllerImp extends PendingOrdersController {
  final state = PendingOrdersState();

  @override
  void onInit() {
    initData();
    getOrders();
    super.onInit();
  }

  @override
  void initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    super.onInit();
  }

  @override
  getOrders() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.ordersData.getPendingOrders(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.orders.addAll(response['data'].map((e) => Order.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  deleteOrder(orderId) async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response =
          await state.ordersData.deleteOrder(orderId, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.orders.removeWhere((element) => element.id == orderId);
        toastMessage(message: 'deleted'.tr);
        update();
        return;
      }
      if (response['msg'] == 'Already Approved') {
        toastMessage(message: 'already_approved'.tr);
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  String getOrderStatus(val) {
    if (val == '0') return 'await_approval'.tr;
    if (val == '1') return 'preparing'.tr;
    if (val == '2') return 'ready_to_deliver'.tr;
    if (val == '3') return 'on_the_way'.tr;
    return 'archived'.tr;
  }

  @override
  String getReceiveType(val) {
    if (val == 'drive_thru') return 'drive_thru'.tr;
    if (val == 'delivery') return 'delivery'.tr;
    return '';
  }

  @override
  String getPaymentType(val) {
    if (val == 'cash') return 'cash_on_delivery'.tr;
    if (val == 'cards') return 'payment_cards'.tr;
    return '';
  }

  @override
  toOrdersDetailsPage(order) {
    Get.toNamed(AppRoute.ordersDetails, arguments: {'order': order});
  }

  @override
  refreshPage() {
    getOrders();
  }
}
