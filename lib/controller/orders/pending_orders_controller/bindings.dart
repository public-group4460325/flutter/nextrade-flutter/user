import 'package:get/get.dart';
import 'package:nextrade/controller/orders/pending_orders_controller/controller.dart';

class PendingOrdersBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PendingOrdersControllerImp>(() => PendingOrdersControllerImp ());
  }
}
