import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/users/orders_data.dart';

class PendingOrdersState {
  List orders = [];
  StatusRequest statusRequest = StatusRequest.none;
  MyServices myServices = Get.find();
  OrdersData ordersData = OrdersData(Get.find());
  late String apiToken;
}
