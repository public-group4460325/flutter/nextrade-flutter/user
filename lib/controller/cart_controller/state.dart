import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/users/cart_data.dart';
import 'package:nextrade/data/models/coupon_model.dart';

class CartState {
  CartData cartData = CartData(Get.find());
  MyServices myServices = Get.find();
  late String apiToken;
  StatusRequest statusRequest = StatusRequest.none;
  List items = [];
  late Coupon coupon;
  double price = 0;
  double priceAfterCoupon = 0;
  double shipping = 20.00;
  late TextEditingController couponController;
}
