import 'package:get/get.dart';
import 'package:nextrade/controller/cart_controller/controller.dart';

class CartBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CartControllerImp>(() => CartControllerImp());
  }
}
