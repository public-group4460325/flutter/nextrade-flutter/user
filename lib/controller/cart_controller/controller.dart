import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/cart_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/cart_item_model.dart';
import 'package:nextrade/data/models/coupon_model.dart';

abstract class CartController extends GetxController {
  initData();
  getData();
  delete(CartItem item);
  save(CartItem item);
  checkCoupon();
  increaseCount(CartItem item);
  decreaseCount(CartItem item);
  toHomePage();
  toCheckOutPage();
}

class CartControllerImp extends CartController {
  final state = CartState();

  @override
  void onInit() {
    initData();
    getData();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.couponController = TextEditingController();
    state.coupon = Coupon.fromJson({'discount': 0});
  }

  @override
  getData() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.cartData.getData(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.items.addAll(response['data'].map((e) => CartItem.fromJson(e)));
        state.price = 0;
        state.items.forEach((element) => state.price +=
            ((element.price - element.price * element.discount / 100) *
                element.count));
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  delete(CartItem item) async {
    try {
      var response =
          await state.cartData.deleteFromCart(item.id!, state.apiToken);
      state.statusRequest = handlingData(response);
      if (response['status']) {
        toastMessage(message: 'deleted'.tr);
        state.items.removeWhere(
          (element) => element.id == item.id,
        );
        state.price = 0;
        state.items.forEach(
            (element) => state.price += (element.price * element.count));
        update();
        return;
      }
      if (response['msg'] == 'Item Not Found') {
        toastMessage(message: 'item_not_found'.tr);
      }
      if (response['msg'] == 'Item Not Found In Cart') {
        toastMessage(message: 'item_not_found_in_cart'.tr);
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
    }
  }

  @override
  save(CartItem item) async {
    try {
      var response = await state.cartData
          .updateCount(item.id!, item.count!, state.apiToken);
      state.statusRequest = handlingData(response);
      if (response['status']) {
        toastMessage(message: 'saved'.tr);
        state.price = 0;
        state.items.forEach((element) => state.price +=
            ((element.price - element.price * element.discount / 100) *
                element.count));
        update();
        return;
      }
      if (response['msg'] == 'Item Not Found') {
        toastMessage(message: 'item_not_found'.tr);
      }
      if (response['msg'] == 'Item Not Found In Cart') {
        toastMessage(message: 'item_not_found_in_cart'.tr);
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
    }
  }

  @override
  checkCoupon() async {
    try {
      var response = await state.cartData
          .checkCoupon(state.apiToken, state.couponController.text);
      state.statusRequest = handlingData(response);
      if (response['status']) {
        toastMessage(message: 'applied'.tr);
        Map<String, dynamic> temp = response['data'];
        state.coupon = Coupon.fromJson(temp);
        state.priceAfterCoupon =
            state.price - (state.price * state.coupon.discount! / 100);
        update();
        return;
      }
      if (response['msg'] == 'Coupon Not Found') {
        toastMessage(message: 'coupon_not_found'.tr);
        state.coupon = Coupon.fromJson({'discount': 0});
        update();
        return;
      }
      if (response['msg'] == 'Coupon Was Expired') {
        toastMessage(message: 'coupon_was_expired'.tr);
        return;
      }
      if (response['msg'] == 'Coupon Was Ended') {
        toastMessage(message: 'coupon_was_ended'.tr);
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      return;
    }
  }

  @override
  increaseCount(item) {
    item.count = (item.count! + 1);
    update();
  }

  @override
  decreaseCount(item) {
    if (item.count! > 0) {
      item.count = (item.count! - 1);
      update();
    }
  }

  @override
  toHomePage() {
    Get.back();
  }

  @override
  toCheckOutPage() {
    if (state.items.isEmpty) return toastMessage(message: 'cart_is_empty'.tr);
    Get.toNamed(
      AppRoute.checkOut,
      arguments: {
        'couponId': state.coupon.id ?? 0,
        'price': state.price,
      },
    );
  }
}
