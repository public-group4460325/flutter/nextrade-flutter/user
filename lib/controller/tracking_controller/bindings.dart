import 'package:get/get.dart';
import 'package:nextrade/controller/tracking_controller/controller.dart';

class TrackingBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TrackingControllerImp>(() => TrackingControllerImp());
  }
}
