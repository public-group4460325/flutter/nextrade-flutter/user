import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nextrade/controller/tracking_controller/state.dart';
import 'package:nextrade/core/functions/get_decode_polyline.dart';

abstract class TrackingController extends GetxController {
  initData();
  initMapData();
  initPolyline();
  getDeliveryLocation();
  updateMarker(double lat, double long);
}

class TrackingControllerImp extends TrackingController {
  final state = TrackingState();

  @override
  void onInit() {
    initData();
    // initPolyline();
    initMapData();
    getDeliveryLocation();
    super.onInit();
  }

  @override
  initData() {
    state.address = Get.arguments['address'];
    state.order = Get.arguments['order'];
  }

  @override
  initMapData() {
    state.kGooglePlex = CameraPosition(
      target: LatLng(state.address.locationLat!, state.address.locationLong!),
      zoom: 12,
    );
    state.markers.add(
      Marker(
        markerId: const MarkerId('current'),
        position:
            LatLng(state.address.locationLat!, state.address.locationLong!),
      ),
    );
  }

  @override
  initPolyline() async {
    Future.delayed(const Duration(seconds: 1));
    state.polylineSet = await getDecodePolyline(
      state.currentLat,
      state.currentLong,
      state.address.locationLat,
      state.address.locationLong,
    );
    update();
  }

  @override
  getDeliveryLocation() {
    FirebaseFirestore.instance
        .collection('delivery')
        .doc('${state.order.id}')
        .snapshots()
        .listen((event) {
      if (event.exists) {
        state.destLat = event.get('lat');
        state.destLong = event.get('long');
      }
      updateMarker(state.destLat!, state.destLong!);
    });
  }

  @override
  updateMarker(lat, long) {
    state.markers.removeWhere((element) => element.mapsId.value == 'dest');
    state.markers.add(
      Marker(
        markerId: const MarkerId('dest'),
        position: LatLng(lat, long),
      ),
    );
    update();
  }
}
