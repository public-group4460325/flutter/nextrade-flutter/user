import 'dart:async';

import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/data/models/address_model.dart';
import 'package:nextrade/data/models/order_model.dart';

class TrackingState {
  StatusRequest statusRequest = StatusRequest.success;
  late Address address;
  late Order order;
  Set<Polyline> polylineSet = {};
  StreamSubscription<Position>? cameraPosition;
  List<Marker> markers = [];
  double? currentLat;
  double? currentLong;
  double? destLat;
  double? destLong;
  late GoogleMapController? controller;
  CameraPosition kGooglePlex =
      const CameraPosition(target: LatLng(0, 0), zoom: 14.4746);
}
