import 'package:get/get.dart';
import 'package:nextrade/controller/favorites_controller/controller.dart';

class FavoritesBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<FavoritesControllerImp>(() => FavoritesControllerImp());
  }
}
