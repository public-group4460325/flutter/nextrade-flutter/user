import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/users/favorites_data.dart';
import 'package:nextrade/data/data_source/remote/users/home_data.dart';

class FavoritesState {
  MyServices myServices = Get.find();
  late String apiToken;
  StatusRequest statusRequest = StatusRequest.none;
  FavoritesData favoritesData = FavoritesData(Get.find());
  List items = [];
  List searchItems = [];
  late TextEditingController searchController;
  bool isSearching = false;
  HomeData homeData = HomeData(Get.find());
}
