import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/favorites_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/item_model.dart';

abstract class FavoritesController extends GetxController {
  initData();
  getData();
  addOrRemoveFromFavorites(int itemId);
  toItemDetailsPage(Item item);
  searchData();
  checkSearch(String value);
  onSearch();
  toNotificationsPage();
}

class FavoritesControllerImp extends FavoritesController {
  final state = FavoritesState();

  @override
  void onInit() {
    initData();
    getData();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.searchController = TextEditingController();
  }

  @override
  getData() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.favoritesData.getData(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.items.addAll(response['data'].map((e) => Item.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  addOrRemoveFromFavorites(itemId) async {
    try {
      var response = await state.favoritesData
          .addOrRemoveFavoritesData(itemId, state.apiToken);
      if (response['status']) {
        if (state.isSearching) {
          if (state.items.isNotEmpty) {
            state.items.removeWhere((element) => element.id == itemId);
          }
          if (state.searchItems.isNotEmpty) {
            Item searchItem =
                state.searchItems.firstWhere((element) => element.id == itemId);
            searchItem.favorite = !searchItem.favorite!;
            if (searchItem.favorite!) state.items.add(searchItem);
            toastMessage(
              message: searchItem.favorite! ? 'added'.tr : 'removed'.tr,
            );
          }
          update();
          return;
        }
        state.items.removeWhere((element) => element.id == itemId);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toItemDetailsPage(Item item) {
    Get.toNamed(AppRoute.itemDetails, arguments: {'item': item});
  }

  @override
  searchData() async {
    try {
      state.statusRequest = StatusRequest.loading;
      var response = await state.homeData
          .searchData(state.searchController.text, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.searchItems.clear();
        state.searchItems.addAll(response['data'].map((e) => Item.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  checkSearch(value) {
    if (value.isEmpty) {
      state.isSearching = false;
      state.statusRequest = StatusRequest.none;
      update();
    }
  }

  @override
  onSearch() {
    state.isSearching = true;
    update();
    searchData();
  }

  @override
  toNotificationsPage() {
    Get.toNamed(AppRoute.notifications);
  }
}
