import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/users/address_data.dart';
import 'package:nextrade/data/data_source/remote/users/check_out_data.dart';
import 'package:nextrade/data/models/radio_model.dart';

class CheckOutState {
  List<Map> radioItems = [
    {
      'radioModel': RadioModel(isSelected: true, value: 0),
      'text': 'cash_on_delivery'.tr,
      'value': 'cash',
    },
    {
      'radioModel': RadioModel(isSelected: false, value: 1),
      'text': 'payment_cards'.tr,
      'value': 'card',
    },
  ];
  int selectedPaymentValue = 0;
  String paymentMethod = 'cash';
  String deliveryType =   'drive_thru';
  int? addressId;
  StatusRequest statusRequest = StatusRequest.none;
  AddressData addressData = AddressData(Get.find());
  CheckOutData checkOutData = CheckOutData(Get.find());
  late String apiToken;
  MyServices myServices = Get.find();
  List addresses = [];
  late int couponId;
  late double price;
}
