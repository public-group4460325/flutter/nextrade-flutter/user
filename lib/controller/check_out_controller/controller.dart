import 'package:get/get.dart';
import 'package:nextrade/controller/check_out_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/address_model.dart';

abstract class CheckOutController extends GetxController {
  initData();
  choosePaymentMethod(int value);
  chooseDeliveryType(String deliveryType);
  chooseAddress(int addressId);
  getAddresses();
  checkOut();
}

class CheckOutControllerImp extends CheckOutController {
  final state = CheckOutState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.couponId = Get.arguments['couponId'];
    state.price = Get.arguments['price'];
  }

  @override
  choosePaymentMethod(value) {
    state.selectedPaymentValue = value;
    state.radioItems.forEach(
      (element) {
        element['radioModel'].isSelected = element['radioModel'].value == value;
      },
    );
    state.paymentMethod = state.radioItems[value]['value'];
    update();
  }

  @override
  chooseDeliveryType(deliveryType) {
    state.deliveryType = deliveryType;
    if (state.deliveryType == 'delivery') {
      getAddresses();
    } else {
      state.addressId = null;
    }
    update();
  }

  @override
  chooseAddress(addressId) {
    state.addressId = addressId;
    update();
  }

  @override
  getAddresses() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.addressData.getData(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        List temp = response['data'];
        state.addresses.clear();
        state.addresses.addAll(temp.map((e) => Address.fromJson(e)));
        state.addressId = state.addresses[0].id;
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  checkOut() async {
    try {
      if (state.addresses.isEmpty && state.deliveryType == 'delivery') {
        return toastMessage(message: 'please_add_an_address'.tr);
      }
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.checkOutData.checkOut(
        addressId: state.addressId,
        paymentType: state.paymentMethod,
        receiveType: state.deliveryType,
        shipping: 10,
        price: state.price,
        couponId: state.couponId,
        apiToken: state.apiToken,
      );
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        toastMessage(message: 'order_sent'.tr);
        Get.offAllNamed(AppRoute.homeScreen);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }
}
