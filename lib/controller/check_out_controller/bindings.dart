import 'package:get/get.dart';
import 'package:nextrade/controller/check_out_controller/controller.dart';

class CheckOutBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<CheckOutControllerImp>(() => CheckOutControllerImp());
  }
}
