import 'package:get/get.dart';

import '../../../core/constants/routes.dart';

abstract class SuccessSignUpController extends GetxController {
  toLoginPage();
}

class SuccessSignUpControllerImp extends SuccessSignUpController {
  @override
  toLoginPage() {
    Get.offAllNamed(AppRoute.login);
  }
}
