import 'package:get/get.dart';

import 'controller.dart';

class SuccessSignUpBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SuccessSignUpControllerImp>(() => SuccessSignUpControllerImp());
  }
}
