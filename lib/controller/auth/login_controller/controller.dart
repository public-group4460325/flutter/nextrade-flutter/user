import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

import '../../../core/classes/status_request.dart';
import '../../../core/constants/routes.dart';
import '../../../core/functions/handling_data.dart';
import '../../../core/functions/toast_message.dart';
import 'state.dart';

abstract class LoginController extends GetxController {
  login();
  toSignUpPage();
  toForgetPasswordPage();
  toVerifyCode(String email);
  showOrHidePassword();
  initData();
}

class LoginControllerImp extends LoginController {
  final state = LoginState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.emailController = TextEditingController();
    state.passwordController = TextEditingController();
  }

  @override
  login() async {
    try {
      if (!state.formState.currentState!.validate()) return;
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.loginData.postData(
        email: state.emailController.text,
        password: state.passwordController.text,
      );
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        if (response['data']['user']['user_type_id'] == 'admin') {
          toastMessage(message: 'download_admins_app'.tr);
          state.statusRequest = StatusRequest.none;
          update();
          return;
        } else if (response['data']['user']['user_type_id'] == 'delivery') {
          toastMessage(message: 'download_deliveries_app'.tr);
          state.statusRequest = StatusRequest.none;
          update();
          return;
        }
        state.myServices.sharedPreferences
            .setString('apiToken', response['data']['token']);
        state.myServices.sharedPreferences
            .setString('id', response['data']['user']['id'].toString());
        state.myServices.sharedPreferences
            .setString('name', response['data']['user']['name']);
        state.myServices.sharedPreferences
            .setString('email', response['data']['user']['email']);
        state.myServices.sharedPreferences.setBool('isLoggedIn', true);
        return;
      }
      if (response['msg'] == 'User Not Found') {
        toastMessage(message: 'user_not_found'.tr);
        update();
        return;
      }
      if (response['msg'] == 'Email Not Verified') {
        toVerifyCode(state.emailController.text);
        toastMessage(message: 'not_verified'.tr);
        update();
        return;
      }
      if (response['msg'] == 'Wrong Password') {
        toastMessage(message: 'wrong_password'.tr);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toSignUpPage() {
    Get.offAllNamed(AppRoute.signUp);
  }

  @override
  toForgetPasswordPage() {
    Get.toNamed(AppRoute.forgetPassword);
  }

  @override
  toVerifyCode(email) {
    Get.toNamed(AppRoute.verifyCodeSignUp, arguments: {'email': email});
  }

  @override
  showOrHidePassword() {
    state.ishHidden = !state.ishHidden;
    update();
  }

  @override
  void dispose() {
    state.emailController.dispose();
    state.passwordController.dispose();
    super.dispose();
  }
}
