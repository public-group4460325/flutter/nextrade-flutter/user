import 'package:get/get.dart';

import '../../../core/classes/status_request.dart';
import '../../../data/data_source/remote/auth/verify_code_sign_up_data.dart';

class VerifySignUpCodeState {
  late String email;
  StatusRequest statusRequest = StatusRequest.none;
  VerifyCodeSignUpData verifyCodeSignUpData = VerifyCodeSignUpData(Get.find());
}
