import 'package:get/get.dart';

import '../../../core/classes/status_request.dart';
import '../../../core/constants/routes.dart';
import '../../../core/functions/handling_data.dart';
import '../../../core/functions/toast_message.dart';
import 'state.dart';

abstract class VerifyCodeSignUpController extends GetxController {
  initData();
  checkCode(String verifyCode);
  toSuccessSignUpPasswordPage();
  resendVerificationCode();
}

class VerifyCodeSignUpControllerImp extends VerifyCodeSignUpController {
  final state = VerifySignUpCodeState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.email = Get.arguments['email'];
  }

  @override
  checkCode(verifyCode) async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.verifyCodeSignUpData.postData(
        email: state.email,
        verificationCode: verifyCode,
      );
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        toSuccessSignUpPasswordPage();
        return;
      }
      if (response['msg'] == 'User Not Found') {
        toastMessage(message: 'user_not_found'.tr);
        update();
        return;
      }
      if (response['msg'] == 'Verification Code Not Match') {
        toastMessage(message: 'verification_code_not_match'.tr);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toSuccessSignUpPasswordPage() {
    Get.offAllNamed(AppRoute.successSignUp);
  }

  @override
  resendVerificationCode() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.verifyCodeSignUpData
          .resendVerificationCode(email: state.email);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        update();
        return;
      }
      if (response['msg'] == 'User Not Found') {
        toastMessage(message: 'user_not_found'.tr);
        update();
        return;
      }
      if (response['msg'] == 'Verification Code Not Match') {
        toastMessage(message: 'verification_code_not_match'.tr);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }
}
