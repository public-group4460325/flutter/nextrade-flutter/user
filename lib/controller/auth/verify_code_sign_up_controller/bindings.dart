import 'package:get/get.dart';

import 'controller.dart';

class VerifyCodeSignUpBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<VerifyCodeSignUpControllerImp>(() => VerifyCodeSignUpControllerImp());
  }
}
