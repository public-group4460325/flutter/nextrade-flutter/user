import 'package:get/get.dart';

import 'controller.dart';

class SignUpBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<SignUpControllerImp>(() => SignUpControllerImp());
  }

}