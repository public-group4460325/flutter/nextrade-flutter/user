import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../core/classes/status_request.dart';
import '../../../core/constants/routes.dart';
import '../../../core/functions/handling_data.dart';
import '../../../core/functions/toast_message.dart';
import 'state.dart';

abstract class SignUpController extends GetxController {
  initData();
  signUp();
  toLoginPage();
  toVerifyCode(String email);
  showOrHidePassword();
}

class SignUpControllerImp extends SignUpController {
  final state = SignUpState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.userNameController = TextEditingController();
    state.emailController = TextEditingController();
    state.confirmPasswordController = TextEditingController();
    state.passwordController = TextEditingController();
  }

  @override
  signUp() async {
    try {
      if (!state.formState.currentState!.validate()) return;
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.signUpData.postData(
        name: state.userNameController.text,
        email: state.emailController.text,
        password: state.passwordController.text,
      );
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        toVerifyCode(state.emailController.text);
        return;
      }
      if (response['msg'] == 'User Was Already Exist') {
        toastMessage(message: 'user_already_exist'.tr);
        update();
        return;
      }
      if (response['msg'] == 'The email has already been taken.') {
        toastMessage(message: 'email_already_exist'.tr);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toLoginPage() {
    Get.offAllNamed(AppRoute.login);
  }

  @override
  toVerifyCode(email) {
    Get.toNamed(AppRoute.verifyCodeSignUp, arguments: {'email': email});
  }

  @override
  showOrHidePassword() {
    state.ishHidden = !state.ishHidden;
    update();
  }

  @override
  void dispose() {
    state.userNameController.dispose();
    state.emailController.dispose();
    state.confirmPasswordController.dispose();
    state.passwordController.dispose();
    super.dispose();
  }
}
