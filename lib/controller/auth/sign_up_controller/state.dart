import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../core/classes/status_request.dart';
import '../../../data/data_source/remote/auth/sign_up_data.dart';

class SignUpState {
  GlobalKey<FormState> formState = GlobalKey<FormState>();
  late TextEditingController userNameController;
  late TextEditingController emailController;
  late TextEditingController passwordController;
  late TextEditingController confirmPasswordController;
  StatusRequest statusRequest = StatusRequest.none;
  SignUpData signUpData = SignUpData(Get.find());
  bool ishHidden = true;
}
