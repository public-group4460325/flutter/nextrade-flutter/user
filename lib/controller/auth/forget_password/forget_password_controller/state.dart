import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../../../core/classes/status_request.dart';
import '../../../../data/data_source/remote/auth/forget_password/check_email_data.dart';

class ForgetPasswordState {
  GlobalKey<FormState> formState = GlobalKey<FormState>();
  late TextEditingController emailController;
  CheckEmailData checkEmailData = CheckEmailData(Get.find());
  StatusRequest statusRequest = StatusRequest.none;
}
