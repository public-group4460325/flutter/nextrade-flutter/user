import 'package:get/get.dart';

import 'controller.dart';

class ForgetPasswordBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ForgetPasswordControllerImp>(
        () => ForgetPasswordControllerImp());
  }
}
