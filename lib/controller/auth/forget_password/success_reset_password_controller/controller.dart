import 'package:get/get.dart';

import '../../../../core/constants/routes.dart';

abstract class SuccessResetPasswordController extends GetxController {
  toLoginPage();
}

class SuccessResetPasswordControllerImp extends SuccessResetPasswordController {
  @override
  toLoginPage() {
    Get.offAllNamed(AppRoute.login);
  }
}
