import 'package:get/get.dart';

import 'controller.dart';

class ResetPasswordBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<ResetPasswordControllerImp>(() => ResetPasswordControllerImp());
  }
}
