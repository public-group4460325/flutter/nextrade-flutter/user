import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';

import '../../../../core/classes/status_request.dart';
import '../../../../core/constants/routes.dart';
import '../../../../core/functions/handling_data.dart';
import '../../../../core/functions/toast_message.dart';
import 'state.dart';

abstract class ResetPasswordController extends GetxController {
  initData();
  resetPassword();
  toSuccessResetPasswordPage();
  showOrHidePassword();
}

class ResetPasswordControllerImp extends ResetPasswordController {
  final state = ResetPasswordState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.passwordController = TextEditingController();
    state.confirmPasswordController = TextEditingController();
    state.email = Get.arguments['email'];
  }

  @override
  resetPassword() async {
    try {
      if (!state.formState.currentState!.validate()) return;
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.resetPasswordData.postData(
        email: Get.arguments['email'],
        password: state.passwordController.text,
      );
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        toSuccessResetPasswordPage();
        return;
      }
      if (response['msg'] == 'User Not Found') {
        toastMessage(message: 'user_not_found'.tr);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: '${'an_error_accord'.tr}: $e');
      update();
    }
  }

  @override
  toSuccessResetPasswordPage() {
    Get.offAllNamed(AppRoute.successResetPassword);
  }

  @override
  showOrHidePassword() {
    state.ishHidden = !state.ishHidden;
    update();
  }

  @override
  void dispose() {
    state.passwordController.dispose();
    state.confirmPasswordController.dispose();
    super.dispose();
  }
}
