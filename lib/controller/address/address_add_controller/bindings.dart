import 'package:get/get.dart';
import 'package:nextrade/controller/address/address_add_controller/controller.dart';

class AddressAddBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddressAddControllerImp>(() => AddressAddControllerImp());
  }
}
