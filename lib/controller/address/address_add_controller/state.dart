import 'dart:async';

import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import '../../../../core/classes/status_request.dart';

class AddressAddState {
  late Position position;
  CameraPosition kGooglePlex =
      const CameraPosition(target: LatLng(0, 0), zoom: 14.4746);
  late Completer<GoogleMapController> controller;
  late CameraPosition kLake;
  late StatusRequest statusRequest;
  List<Marker> markers = [];
  double? lat;
  double? long;
}
