import 'dart:async';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nextrade/controller/address/address_add_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/get_current_location.dart';
abstract class AddressAddController extends GetxController {
  initData();
  addMarkers(LatLng latLng);
  toAddressAddPartTwo();
}

class AddressAddControllerImp extends AddressAddController {
  final state = AddressAddState();

  @override
  void onInit() async {
    await initData();
    super.onInit();
  }

  @override
  initData() async {
    state.statusRequest = StatusRequest.loading;
    state.position = await getCurrentLocation();
    state.kGooglePlex = CameraPosition(
      target: LatLng(state.position.latitude, state.position.longitude),
      zoom: 14.4746,
    );
    state.controller = Completer<GoogleMapController>();
    // state.kLake = const CameraPosition(
    // bearing: 192.8334901395799,
    // target: LatLng(37.43296265331129, -122.08832357078792),
    // tilt: 59.440717697143555,
    // zoom: 19.151926040649414);
    state.markers.add(
      Marker(
        markerId: const MarkerId('1'),
        position: LatLng(
          state.position.latitude,
          state.position.longitude,
        ),
      ),
    );
    state.lat = state.position.latitude;
    state.long = state.position.longitude;
    state.statusRequest = StatusRequest.none;
    update();
  }

  @override
  addMarkers(latLng) {
    state.markers.clear();
    state.markers.add(
      Marker(
        markerId: const MarkerId('1'),
        position: latLng,
      ),
    );
    state.lat = latLng.latitude;
    state.long = latLng.longitude;
    update();
  }

  @override
  toAddressAddPartTwo() {
    Get.toNamed(
      AppRoute.addressAddDetails,
      arguments: {
        'lat': state.lat.toString(),
        'long': state.long.toString(),
      },
    );
  }
}
