import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/address/address_add_details_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';

abstract class AddressAddDetailsController extends GetxController {
  initData();
  addAddress();
}

class AddressAddDetailsControllerImp extends AddressAddDetailsController {
  final state = AddressAddDetailsState();

  @override
  void onInit() {
    initData();
    super.onInit();
  }

  @override
  initData() {
    state.lat = Get.arguments['lat'];
    state.long = Get.arguments['long'];
    state.statusRequest = StatusRequest.none;
    state.nameController = TextEditingController();
    state.cityController = TextEditingController();
    state.streetController = TextEditingController();
    state.phoneController = TextEditingController();
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    update();
  }

  @override
  addAddress() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.addressData.addData(
        state.apiToken,
        state.nameController.text,
        state.cityController.text,
        state.streetController.text,
        state.phoneController.text,
        state.lat,
        state.long,
      );
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        Get.offAllNamed(AppRoute.homeScreen);
        toastMessage(message: 'location_added'.tr);
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }
}
