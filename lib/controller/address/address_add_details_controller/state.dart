import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/users/address_data.dart';

class AddressAddDetailsState {
  late StatusRequest statusRequest;
  late String lat;
  late String long;
  late TextEditingController nameController;
  late TextEditingController cityController;
  late TextEditingController streetController;
  late TextEditingController phoneController;
  AddressData addressData = AddressData(Get.find());
  late MyServices myServices = Get.find();
  late String apiToken;
}
