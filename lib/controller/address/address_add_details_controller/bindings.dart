import 'package:get/get.dart';
import 'package:nextrade/controller/address/address_add_details_controller/controller.dart';

class AddressAddDetailsBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddressAddDetailsControllerImp>(() => AddressAddDetailsControllerImp());
  }
}
