import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/users/address_data.dart';

class AddressViewState {
  AddressData addressData = AddressData(Get.find());
  MyServices myServices = Get.find();
  List data = [];
  StatusRequest statusRequest = StatusRequest.none;
  late String apiToken;
}
