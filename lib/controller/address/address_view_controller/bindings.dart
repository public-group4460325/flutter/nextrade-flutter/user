import 'package:get/get.dart';
import 'package:nextrade/controller/address/address_view_controller/controller.dart';

class AddressViewBindings extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<AddressViewControllerImp>(() => AddressViewControllerImp());
  }
}
