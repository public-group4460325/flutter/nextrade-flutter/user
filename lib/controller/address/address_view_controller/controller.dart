import 'package:get/get.dart';
import 'package:nextrade/controller/address/address_view_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/address_model.dart';

abstract class AddressViewController extends GetxController {
  initData();
  getData();
  delete(int addressId);
  toAddressAddPage();
}

class AddressViewControllerImp extends AddressViewController {
  final state = AddressViewState();

  @override
  void onInit() {
    initData();
    getData();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
  }

  @override
  getData() async {
    try {
      state.statusRequest = StatusRequest.loading;
      update();
      var response = await state.addressData.getData(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        List temp = response['data'];
        state.data.addAll(temp.map((e) => Address.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  delete(addressId) async {
    try {
      var response = await state.addressData.deleteData(addressId, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.data.removeWhere((element) => element.id == addressId);
        if (state.data.isEmpty) state.statusRequest = StatusRequest.noData;
        update();
        return;
      }
      toastMessage(message: response['msg']);
      update();
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  toAddressAddPage() {
    Get.toNamed(AppRoute.addressAdd);
  }
}
