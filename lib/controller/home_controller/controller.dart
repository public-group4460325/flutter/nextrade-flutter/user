import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/home_controller/state.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/functions/handling_data.dart';
import 'package:nextrade/core/functions/toast_message.dart';
import 'package:nextrade/data/models/category_model.dart';
import 'package:nextrade/data/models/item_model.dart';
import 'package:nextrade/data/models/setting_model.dart';

abstract class HomeController extends GetxController {
  initData();
  getData();
  searchData();
  checkSearch(String value);
  onSearch();
  toItemsPage(List categories, int selectedCategory);
  toItemDetailsPage(Item item);
  toNotificationsPage();
  addOrRemoveFromFavorites(int itemId);
}

class HomeControllerImp extends HomeController {
  final state = HomeState();

  @override
  void onInit() async {
    await initData();
    await getData();
    super.onInit();
  }

  @override
  initData() {
    state.apiToken = state.myServices.sharedPreferences.getString('apiToken')!;
    state.lang = state.myServices.sharedPreferences.getString('lang')!;
    state.searchController = TextEditingController();
  }

  @override
  getData() async {
    try {
      state.statusRequest = StatusRequest.loading;
      var response = await state.homeData.getData(state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.items.addAll(
            response['data']['offers_items'].map((e) => Item.fromJson(e)));
        state.topItems
            .addAll(response['data']['top_items'].map((e) => Item.fromJson(e)));
        state.setting = Setting.fromJson(response['data']['settings'][0]);
        state.categories.addAll(
            response['data']['categories'].map((e) => Category.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  searchData() async {
    try {
      state.statusRequest = StatusRequest.loading;
      var response = await state.homeData
          .searchData(state.searchController.text, state.apiToken);
      state.statusRequest = handlingData(response);
      if (StatusRequest.success != state.statusRequest) {
        update();
        return;
      }
      if (response['status']) {
        state.searchItems.clear();
        state.searchItems.addAll(response['data'].map((e) => Item.fromJson(e)));
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }

  @override
  checkSearch(value) {
    if (value.isEmpty) {
      state.isSearching = false;
      state.statusRequest = StatusRequest.none;
      update();
    }
  }

  @override
  onSearch() {
    state.isSearching = true;
    update();
    searchData();
  }

  @override
  toItemsPage(categories, selectedCategory) {
    Get.toNamed(
      AppRoute.items,
      arguments: {
        'categories': categories,
        'selectedCategory': selectedCategory,
      },
    );
  }

  @override
  toItemDetailsPage(Item item) {
    Get.toNamed(AppRoute.itemDetails, arguments: {'item': item});
  }

  @override
  toNotificationsPage() {
    Get.toNamed(AppRoute.notifications);
  }

  @override
  addOrRemoveFromFavorites(itemId) async {
    try {
      var response = await state.favoritesData
          .addOrRemoveFavoritesData(itemId, state.apiToken);
      if (response['status']) {
        Item item =
            state.searchItems.firstWhere((element) => element.id == itemId);
        item.favorite = !item.favorite!;
        toastMessage(
          message: item.favorite! ? 'added'.tr : 'removed'.tr,
        );
        update();
        return;
      }
      toastMessage(message: response['msg']);
    } catch (e) {
      toastMessage(message: "${'an_error_accord'.tr}: $e");
      update();
    }
  }
}
