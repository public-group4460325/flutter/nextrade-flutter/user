import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/classes/status_request.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/data/data_source/remote/users/favorites_data.dart';
import 'package:nextrade/data/data_source/remote/users/home_data.dart';
import 'package:nextrade/data/models/setting_model.dart';

class HomeState {
  MyServices myServices = Get.find();
  late String apiToken;
  StatusRequest statusRequest = StatusRequest.none;
  HomeData homeData = HomeData(Get.find());
  FavoritesData favoritesData = FavoritesData(Get.find());
  List categories = [];
  List items = [];
  List topItems = [];
  List searchItems = [];
  Setting? setting;
  late String lang;
  late TextEditingController searchController;
  bool isSearching = false;
}
