import 'package:get/get.dart';
import 'package:nextrade/core/classes/crud.dart';

class InitialBindings extends Bindings {
  @override
  void dependencies() {
    Get.put(CRUD());
  }
}
