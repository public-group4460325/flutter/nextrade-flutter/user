import 'package:flutter/material.dart';

import 'color.dart';

class AppTheme {
  static final ThemeData themeDataEnglish = ThemeData(
    appBarTheme: const AppBarTheme(
      
      backgroundColor: AppColor.backGroundColor,
      titleTextStyle: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 20,
        color: AppColor.grey,
      ),
      centerTitle: true,
      elevation: 0,
    ),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      backgroundColor: AppColor.primaryColor,
    ),
    fontFamily: 'PlayfairDisplay',
    textTheme: const TextTheme(
      headlineLarge: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 26,
        color: AppColor.black,
      ),
      headlineMedium: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 20,
        color: AppColor.black,
      ),
      bodyLarge: TextStyle(
        height: 2,
        color: AppColor.grey,
        fontWeight: FontWeight.bold,
        fontSize: 17,
      ),
    ),
  );
  static final ThemeData themeDataArabic = ThemeData(
    appBarTheme: const AppBarTheme(
      backgroundColor: AppColor.backGroundColor,
      titleTextStyle: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 20,
        color: AppColor.grey,
      ),
      centerTitle: true,
      elevation: 0,
    ),
    floatingActionButtonTheme: const FloatingActionButtonThemeData(
      backgroundColor: AppColor.primaryColor,
    ),
    fontFamily: 'Cairo',
    textTheme: const TextTheme(
      headlineLarge: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 26,
        color: AppColor.black,
      ),
      headlineMedium: TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 20,
        color: AppColor.black,
      ),
      bodyLarge: TextStyle(
        height: 2,
        color: AppColor.grey,
        fontWeight: FontWeight.bold,
        fontSize: 17,
      ),
    ),
  );
}
