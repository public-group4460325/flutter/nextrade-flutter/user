class AppImageAsset {
  static const String rootImage = 'assets/images';

  // Auth
  static const String logo = '$rootImage/auth/Logo.png';
  static const String googleLogo = '$rootImage/auth/google.png';
  static const String forgetPassword = '$rootImage/auth/forget_password.png';
  static const String resetPassword = '$rootImage/auth/reset_password.png';
  static const String verifyCode = '$rootImage/auth/verify_code.png';
  static const String successResetPassword =
      '$rootImage/auth/success_reset_password.png';
  static const String checkEmail = '$rootImage/auth/check_email.png';
  // OnBoarding
  static const String onBoardingImageFast = '$rootImage/on_boarding/fast.png';
  static const String onBoardingImageMap = '$rootImage/on_boarding/map.png';
  static const String onBoardingImageSafe = '$rootImage/on_boarding/safe.png';
  static const String onBoardingImageSelect =
      '$rootImage/on_boarding/select.png';

  static const String profile = '$rootImage/profile.png';
  static const String discount = '$rootImage/discount.png';

  static const String location = '$rootImage/location.png';
  
  static const String delivery = '$rootImage/delivery/delivery.png';
  static const String driveThru = '$rootImage/delivery/drive_thru.png';
}
