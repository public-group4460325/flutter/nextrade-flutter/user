class AppRoute {
  // Auth
  static const String login = '/login';
  static const String signUp = '/signUp';
  static const String forgetPassword = '/forgetPassword';
  static const String verifyCodeSignUp = '/verifyCodeSignUp';
  static const String verifyCodeForgetPassword = '/verifyCodeForgetPassword';
  static const String resetPassword = '/resetPassword';
  static const String successSignUp = '/successSignUp';
  static const String successResetPassword = '/successResetPassword';
  // OnBoarding
  static const String onBoarding = '/onBoarding';
  static const String language = '/language';
  // Home
  static const String homeScreen = '/homeScreen';
  static const String home = '/home';
  // Items
  static const String items = '/items';
  static const String itemDetails = '/itemDetails';

  static const String favorites = '/favorites';
  static const String cart = '/cart';

  // Addresses
  static const String addressView = '/addressView';
  static const String addressAdd = '/addressAdd';
  static const String addressAddDetails = '/addressAddDetails';

  static const String tracking = '/tracking';

  static const String checkOut = '/checkOut';
  static const String orders = '/orders';
  static const String archivedOrders = '/archivedOrders';
  static const String ordersDetails = '/ordersDetails';

  static const String notifications = '/notifications';
}
