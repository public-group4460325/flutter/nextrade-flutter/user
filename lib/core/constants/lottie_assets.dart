class AppLottieAsset {
  static const String rootLottie = 'assets/lottie';

  static const String loading = '$rootLottie/loading.json';
  static const String error = '$rootLottie/error.json';
  static const String noDataFound = '$rootLottie/no_data_found.json';
  static const String connectionLost = '$rootLottie/connection_lost.json';
}
