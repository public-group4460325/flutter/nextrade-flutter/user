class AppLinkServer {
  // Server
  static const String server = "http://10.0.2.2:8000";
  static const String auth = "$server/api/auth";
  static const String users = "$server/api/users";
  static const String storage = "$server/storage";

  // ================================= Auth ==================================
  static const String signUp = '$auth/signUp';
  static const String verifyCodeSignUp = '$auth/verifyCodeSignUp';
  static const String login = '$auth/login';
  static const String logout = '$auth/logout';
  static const String resendVerificationCode = '$auth/resendVerificationCode';

  // Forget Password
  static const String resetPassword = '$auth/resetPassword';
  static const String checkEmail = '$auth/checkEmail';
  static const String verifyCodeForgetPassword =
      '$auth/verifyCodeForgetPassword';
  // ========================================================================

  // ================================= Users =================================
  // Home
  static const String getHomeData = '$users/categories/getHomeData';

  // Items
  static const String getItemsByCategory = '$users/items/getItems';
  static const String search = '$users/items/search';

  // Favorites
  static const String addOrRemoveFromFavorites = '$users/favorites/addOrRemove';
  static const String getMyFavorites = '$users/favorites/get';

  // Cart
  static const String addToCart = '$users/cart/add';
  static const String removeFromCart = '$users/cart/remove';
  static const String getAllCartItems = '$users/cart/get';
  static const String updateCount = '$users/cart/update';
  static const String deleteFromCart = '$users/cart/delete';

  // Addresses
  static const String getAddresses = '$users/addresses/get';
  static const String addAddress = '$users/addresses/create';
  static const String updateAddress = '$users/addresses/update';
  static const String deleteAddress = '$users/addresses/delete';

  // Coupons
  static const String checkCoupon = '$users/coupons/check';

  // Orders
  static const String addOrder = '$users/orders/add';
  static const String getPendingOrders = '$users/orders/getPending';
  static const String getArchivedOrders = '$users/orders/getArchived';
  static const String getOrderDetails = '$users/orders/getDetails';
  static const String deleteOrder = '$users/orders/delete';
  static const String rateOrder = '$users/orders/rate';

  // Notifications
  static const String getAllNotifications = '$users/notifications/get';
  // ========================================================================
}
