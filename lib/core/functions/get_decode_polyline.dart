import 'dart:convert';

import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:nextrade/core/constants/color.dart';

Future<Set<Polyline>> getDecodePolyline(
    stateLat, startLong, endLat, endLong) async {
  Set<Polyline> polylineSet = {};
  List<LatLng> latLng = [];
  PolylinePoints polylinePoints = PolylinePoints();
  String url =
      'https://maps.googleapis.com/map s/api/directions/json?origin=$stateLat,$startLong&distination=$endLat,$endLong&key={Google Key}';
  var response = await http.post(Uri.parse(url));
  var responseBody = jsonDecode(response.body);
  var point = responseBody['routes'][0]['overview_polyline']['points'];
  List<PointLatLng> result = polylinePoints.decodePolyline(point);
  if (result.isNotEmpty) {
    result.forEach((PointLatLng pointLatLng) {
      latLng.add(LatLng(pointLatLng.latitude, pointLatLng.longitude));
    });
  }

  Polyline polyline = Polyline(
    polylineId: PolylineId('dest'),
    color: AppColor.primaryColor,
    width: 3,
    points: latLng,
  );
  polylineSet.add(polyline);
  return polylineSet;
}
