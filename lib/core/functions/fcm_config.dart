import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/controller/notifications_controller/controller.dart';
import 'package:nextrade/controller/orders/pending_orders_controller/controller.dart';

requestNotificationsPermission() async {
  await FirebaseMessaging.instance.requestPermission(
    alert: true,
    announcement: false,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );
}

fcmConfig() async {
  FirebaseMessaging.onMessage.listen(_firebaseMessagingForegroundHandler);
  // FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
}

refreshPages(data) {
  if (data == null) return;
  String currentRoute = Get.currentRoute;
  if (currentRoute == '/orders' && data['page_name'] == 'order') {
    PendingOrdersControllerImp? controller = Get.find();
    controller!.refreshPage();
  } else if (currentRoute == '/notifications' && data['page_name'] == 'order') {
    NotificationsControllerImp? controller = Get.find();
    controller!.refreshPage();
  }
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('@mipmap/ic_launcher');
  const InitializationSettings initializationSettings =
      InitializationSettings(android: initializationSettingsAndroid);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  AndroidNotificationDetails androidPlatformChannelSpecifics =
      const AndroidNotificationDetails(
    '1',
    'notifications',
    importance: Importance.max,
    priority: Priority.max,
    showWhen: true,
    colorized: true,
    color: AppColor.primaryColor,
    ledColor: AppColor.primaryColor,
    ledOnMs: 1000,
    ledOffMs: 500,
    ticker: 'nexTrade',
  );
  NotificationDetails platformChannelSpecifics =
      NotificationDetails(android: androidPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
    0,
    '${message.notification!.title}',
    '${message.notification!.body}',
    platformChannelSpecifics,
  );
}

void _firebaseMessagingForegroundHandler(RemoteMessage message) async {
  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();
  const AndroidInitializationSettings initializationSettingsAndroid =
      AndroidInitializationSettings('@mipmap/ic_launcher');
  const InitializationSettings initializationSettings =
      InitializationSettings(android: initializationSettingsAndroid);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings);
  AndroidNotificationDetails androidPlatformChannelSpecifics =
      const AndroidNotificationDetails(
    '1',
    'notifications',
    importance: Importance.max,
    priority: Priority.max,
    showWhen: true,
    colorized: true,
    color: AppColor.primaryColor,
    ledColor: AppColor.primaryColor,
    ledOnMs: 1000,
    ledOffMs: 500,
    ticker: 'nexTrade',
  );
  NotificationDetails platformChannelSpecifics =
      NotificationDetails(android: androidPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(
    0,
    '${message.notification!.title}',
    '${message.notification!.body}',
    platformChannelSpecifics,
  );
  refreshPages(message.data);
}
