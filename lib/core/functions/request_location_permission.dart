import 'package:geolocator/geolocator.dart';
import 'package:get/get.dart';

import 'toast_message.dart';

requestLocationPermission() async {
  bool serviceEnabled;
  LocationPermission permission;
  serviceEnabled = await Geolocator.isLocationServiceEnabled();
  if (!serviceEnabled) {
    return toastMessage(message: 'please_active_location'.tr);
  }
  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      return toastMessage(message: 'please_give_permission'.tr);
    }
  }
  if (permission == LocationPermission.deniedForever) {
    return toastMessage(message: 'cant_use_app_without_location'.tr);
  }
}
