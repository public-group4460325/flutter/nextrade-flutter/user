import 'package:get/get.dart';

validationInput(String val, int min, int max, String type) {
  if (val.isEmpty) return 'cant_be_empty'.tr;
  if (val.length < min) return 'cant_be_less_than'.tr + min.toString() + ''.tr;
  if (val.length > max) return 'cant_be_more_than'.tr + max.toString() + ''.tr;
  if (type == 'userName') {
    if (!GetUtils.isUsername(val)) return 'not_valid_username'.tr;
  }
  if (type == 'email') {
    if (!GetUtils.isEmail(val)) return 'not_valid_email'.tr;
  }
  if (type == 'phone') {
    if (!GetUtils.isPhoneNumber(val)) return 'not_valid_phone'.tr;
  }
}
