import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/color.dart';

Future<bool> ExitAlertApp() {
  Get.defaultDialog(
    title: 'Alert',
    middleText: 'Do You Want To Exit The App',
    actions: [
      MaterialButton(
        shape: RoundedRectangleBorder(
          side: const BorderSide(
            color: AppColor.primaryColor,
            width: 3,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
        textColor: AppColor.primaryColor,
        color: AppColor.backGroundColor,
        onPressed: () {
          exit(0);
        },
        child: Text('confirm'.tr),
      ),
      MaterialButton(
        shape: RoundedRectangleBorder(
          side: const BorderSide(
            color: AppColor.primaryColor,
            width: 3,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
        textColor: AppColor.primaryColor,
        color: AppColor.backGroundColor,
        onPressed: () {
          Get.back();
        },
        child: Text('cancel'.tr),
      ),
    ],
  );
  return Future.value(true);
}
