import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/orders/archived_orders_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/image_assets.dart';
import 'package:rating_dialog/rating_dialog.dart';

void showRatingDialog(BuildContext context, int orderId) {
  showDialog(
    context: context,
    barrierDismissible: true,
    builder: (context) => RatingDialog(
      initialRating: 1.0,
      title: const Text(
        'nexTrade',
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 25,
          fontWeight: FontWeight.bold,
        ),
      ),
      message: Text(
        'rating_comment'.tr,
        textAlign: TextAlign.center,
        style: const TextStyle(fontSize: 15),
      ),
      image: Image.asset(
        AppImageAsset.logo,
        width: 200,
        height: 200,
      ),
      submitButtonText: 'submit'.tr,
      submitButtonTextStyle: const TextStyle(
        color: AppColor.primaryColor,
        fontWeight: FontWeight.bold,
      ),
      commentHint: 'enter_your_comment'.tr,
      onSubmitted: (response) async {
        ArchivedOrdersControllerImp controller = Get.find();
        controller.sendRating(orderId, response.rating, response.comment);
      },
    ),
  );
}
