import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/services/my_services.dart';

class MyMiddleware extends GetMiddleware {
  @override
  int? get priority => 1;

  @override
  RouteSettings? redirect(String? route) {
    MyServices myServices = Get.find();
    if (myServices.sharedPreferences.getBool('isFirstTime') != null &&
        !myServices.sharedPreferences.getBool('isFirstTime')!) {
      return const RouteSettings(name: AppRoute.login);
    }
    if (myServices.sharedPreferences.getBool('isLoggedIn') != null &&
        myServices.sharedPreferences.getBool('isLoggedIn')!) {
      return const RouteSettings(name: AppRoute.homeScreen);
    }
    return null;
  }
}
