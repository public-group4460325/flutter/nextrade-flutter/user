import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/theme.dart';
import 'package:nextrade/core/services/my_services.dart';

class ChangeLocaleState {
  ThemeData appTheme = Get.deviceLocale!.languageCode == 'ar'
      ? AppTheme.themeDataArabic
      : AppTheme.themeDataEnglish;
  Locale? language;
  MyServices myServices = Get.find();
}
