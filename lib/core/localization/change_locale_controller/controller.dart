import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/routes.dart';
import 'package:nextrade/core/constants/theme.dart';
import 'package:nextrade/core/functions/fcm_config.dart';
import 'package:nextrade/core/functions/request_location_permission.dart';
import 'package:nextrade/core/localization/change_locale_controller/state.dart';

abstract class ChangeLocaleController extends GetxController {
  changeLang(String langCode);
  toOnBoardingPage();
  initFunctions();
}

class ChangeLocaleControllerImp extends ChangeLocaleController {
  final state = ChangeLocaleState();

  @override
  void onInit() {
    initFunctions();
    String? sharedPrefLang =
        state.myServices.sharedPreferences.getString('lang');
    if (sharedPrefLang == 'ar') {
      state.language = const Locale('ar');
      state.appTheme = AppTheme.themeDataArabic;
    } else if (sharedPrefLang == 'en') {
      state.language = const Locale('en');
    } else {
      state.language = Locale(Get.deviceLocale!.languageCode);
      state.appTheme = AppTheme.themeDataEnglish;
    }
    super.onInit();
  }

  @override
  changeLang(String langCode) {
    Locale locale = Locale(langCode);
    state.myServices.sharedPreferences.setString('lang', langCode);
    state.appTheme = langCode == 'ar'
        ? state.appTheme = AppTheme.themeDataArabic
        : state.appTheme = AppTheme.themeDataEnglish;
    Get.updateLocale(locale);
  }

  @override
  toOnBoardingPage() {
    Get.offNamed(AppRoute.onBoarding);
  }

  @override
  initFunctions() async {
    await requestNotificationsPermission();
    await fcmConfig();
    await requestLocationPermission();
  }
}
