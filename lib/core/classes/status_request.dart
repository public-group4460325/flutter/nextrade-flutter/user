enum StatusRequest {
  none,
  noData,
  loading,
  success,
  failed,
  serverFailed,
  offLineFailed,
  serverException,
}
