import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/check_out_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/image_assets.dart';
import 'package:nextrade/view/widget/check_out/custom_bottom_nav_bar.dart';
import 'package:nextrade/view/widget/check_out/custom_choose_delivery_address.dart';
import 'package:nextrade/view/widget/check_out/custom_choose_delivery_type.dart';
import 'package:nextrade/view/widget/check_out/custom_choose_payment_method_radio.dart';
import 'package:nextrade/view/widget/check_out/custom_title.dart';

class CheckOut extends GetView<CheckOutControllerImp> {
  const CheckOut({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('check_out'.tr)),
      bottomNavigationBar: CustomBottomNavBar(
        onPressed: () => controller.checkOut(),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: ListView(
          children: [
            CustomTitle(title: 'choose_payment_method'.tr),
            GetBuilder<CheckOutControllerImp>(
              builder: (controller) => Row(
                children: List.generate(
                  controller.state.radioItems.length,
                  (index) => CustomChoosePaymentMethodRadio(
                    title: '${controller.state.radioItems[index]['text']}',
                    onChanged: (value) =>
                        controller.choosePaymentMethod(value!),
                    radio: controller.state.radioItems[index]['radioModel'],
                    selectedValue: controller.state.selectedPaymentValue,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 20),
            CustomTitle(title: 'choose_delivery_type'.tr),
            const SizedBox(height: 20),
            GetBuilder<CheckOutControllerImp>(
              builder: (controller) => Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CustomChooseDeliveryType(
                      onTap: () => controller.chooseDeliveryType('delivery'),
                      title: 'delivery'.tr,
                      imageUrl: AppImageAsset.delivery,
                      isActive: controller.state.deliveryType == 'delivery',
                    ),
                    CustomChooseDeliveryType(
                      onTap: () => controller.chooseDeliveryType('drive_thru'),
                      title: 'drive_thru'.tr,
                      imageUrl: AppImageAsset.driveThru,
                      isActive: controller.state.deliveryType == 'drive_thru',
                    ),
                  ],
                ),
              ),
            ),
            const SizedBox(height: 20),
            GetBuilder<CheckOutControllerImp>(
              builder: (controller) =>
                  controller.state.deliveryType == 'delivery'
                      ? HandlingDataView(
                          statusRequest: controller.state.statusRequest,
                          widget: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomTitle(title: 'choose_delivery_address'.tr),
                              const SizedBox(height: 20),
                              ...List.generate(
                                controller.state.addresses.length,
                                (index) => CustomChooseDeliveryAddress(
                                  address: controller.state.addresses[index],
                                  isActive:
                                      controller.state.addresses[index].id ==
                                          controller.state.addressId,
                                  onTap: () => controller.chooseAddress(
                                    controller.state.addresses[index].id,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      : Container(),
            ),
          ],
        ),
      ),
    );
  }
}
