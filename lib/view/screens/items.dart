import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/controller/items_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/view/widget/custom_search_app_bar.dart';
import 'package:nextrade/view/widget/custom_search_items_card.dart';
import 'package:nextrade/view/widget/items/custom_categories_list.dart';
import 'package:nextrade/view/widget/items/custom_items_list.dart';

class Items extends GetView<ItemsControllerImp> {
  const Items({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              CustomSearchAppBar(
                searchController: controller.state.searchController,
                title: 'search'.tr,
                onNotificationsPressed: () => controller.toNotificationsPage(),
                onSearchPressed: () => controller.onSearch(),
                onChanged: (value) => controller.checkSearch(value),
              ),
              Expanded(
                child: GetBuilder<ItemsControllerImp>(
                  builder: (controller) => ListView(
                    physics: const BouncingScrollPhysics(),
                    children: [
                      controller.state.isSearching
                          ? HandlingDataView(
                              statusRequest: controller.state.statusRequest,
                              widget: ListView.builder(
                                shrinkWrap: true,
                                physics: const NeverScrollableScrollPhysics(),
                                itemCount: controller.state.searchItems.length,
                                itemBuilder: (context, index) =>
                                    CustomSearchItemsCard(
                                  item: controller.state.searchItems[index],
                                  onIconPressed: () =>
                                      controller.addOrRemoveFromFavorites(
                                    controller.state.searchItems[index].id,
                                  ),
                                  onTap: () => controller.toItemDetailsPage(
                                    controller.state.searchItems[index],
                                  ),
                                  icon: GetBuilder<ItemsControllerImp>(
                                    builder: (controller) => Icon(
                                      controller.state.searchItems[index]
                                              .favorite!
                                          ? LineIcons.heartAlt
                                          : LineIcons.heart,
                                      color: Colors.red,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : Column(
                              children: [
                                const CustomCategoriesList(),
                                HandlingDataView(
                                  statusRequest: controller.state.statusRequest,
                                  widget: GridView.builder(
                                    shrinkWrap: true,
                                    physics:
                                        const NeverScrollableScrollPhysics(),
                                    itemCount: controller.state.items.length,
                                    gridDelegate:
                                        const SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 2,
                                      childAspectRatio: 0.68,
                                    ),
                                    itemBuilder: (context, index) {
                                      return CustomItemsList(
                                        favorite: controller
                                            .state.items[index].favorite,
                                        item: controller.state.items[index],
                                      );
                                    },
                                  ),
                                ),
                              ],
                            ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
