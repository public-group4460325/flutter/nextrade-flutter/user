import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/onboarding_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/view/widget/on_boarding/custom_continue_button.dart';
import 'package:nextrade/view/widget/on_boarding/custom_dot_controller.dart';
import 'package:nextrade/view/widget/on_boarding/custom_skip_button.dart';
import 'package:nextrade/view/widget/on_boarding/custom_slider.dart';

class OnBoarding extends GetView<OnBoardingControllerImp> {
  const OnBoarding({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: AppColor.backGroundColor,
      body: SafeArea(
        child: Column(
          children: [
            Expanded(flex: 2, child: CustomSlider()),
            Expanded(
              flex: 1,
              child: Column(
                children: [
                  CustomDotController(),
                  Spacer(flex: 2),
                  CustomContinueButton(),
                  CustomSkipButton(),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
