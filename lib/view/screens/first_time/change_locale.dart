import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/core/localization/change_locale_controller/controller.dart';
import 'package:nextrade/view/widget/change_locale/custom_button_lang.dart';

class Language extends GetView<ChangeLocaleControllerImp> {
  const Language({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      body: Container(
        padding: EdgeInsets.all(AppDimensions.width / 25),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'choose_lang'.tr,
              style: Theme.of(context).textTheme.headlineMedium,
            ),
            const SizedBox(
              height: 20,
            ),
            CustomButtonLang(
              textButton: 'ar'.tr,
              onPressed: () {
                controller.changeLang('ar');
                controller.toOnBoardingPage();
              },
            ),
            CustomButtonLang(
              textButton: 'en'.tr,
              onPressed: () {
                controller.changeLang('en');
                controller.toOnBoardingPage();
              },
            ),
          ],
        ),
      ),
    );
  }
}
