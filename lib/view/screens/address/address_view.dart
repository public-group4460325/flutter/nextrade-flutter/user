import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/controller/address/address_view_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/view/widget/address/address_view/custom_address_card.dart';

class AddressView extends GetView<AddressViewControllerImp> {
  const AddressView({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(AddressViewControllerImp());
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('addresses'.tr)),
      floatingActionButton: FloatingActionButton(
        onPressed: () => controller.toAddressAddPage(),
        child: const Icon(
          LineIcons.plus,
        ),
      ),
      body: GetBuilder<AddressViewControllerImp>(
        builder: (controller) => HandlingDataView(
          statusRequest: controller.state.statusRequest,
          widget: Container(
            child: ListView.builder(
              itemCount: controller.state.data.length,
              itemBuilder: (context, index) => CustomAddressCard(
                address: controller.state.data[index],
                onDelete: () => controller.delete(
                  controller.state.data[index].id,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
