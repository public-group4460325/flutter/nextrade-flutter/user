import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/address/address_add_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/shared/custom_button.dart';
import 'package:nextrade/view/widget/address/address_add/custom_map.dart';
class AddressAdd extends GetView<AddressAddControllerImp> {
  const AddressAdd({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('add_new_address'.tr)),
      body: GetBuilder<AddressAddControllerImp>(
        builder: (controller) => HandlingDataView(
          statusRequest: controller.state.statusRequest,
          widget: Column(
            children: [
              Expanded(
                child: Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    const CustomMap(),
                    CustomButton(
                      onPressed: controller.toAddressAddPartTwo(),
                      text: 'continue'.tr,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
