import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/controller/address/address_add_details_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/shared/custom_button.dart';
import 'package:nextrade/core/shared/custom_text_form_field.dart';
import 'package:nextrade/view/widget/address/address_add_details/custom_logo.dart';

class AddressAddDetails extends GetView<AddressAddDetailsControllerImp> {
  const AddressAddDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('add_details_address'.tr)),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: HandlingDataView(
          statusRequest: controller.state.statusRequest,
          widget: GetBuilder<AddressAddDetailsControllerImp>(
            builder: (controller) => ListView(
              children: [
                const CustomLogo(),
                CustomTextFormField(
                  hintText: 'enter_location_name'.tr,
                  labelText: 'name'.tr,
                  icon: LineIcons.locationArrow,
                  textEditingController: controller.state.nameController,
                  validator: (val) {},
                  isNumber: false,
                ),
                CustomTextFormField(
                  hintText: 'enter_city_name'.tr,
                  labelText: 'city'.tr,
                  icon: LineIcons.city,
                  textEditingController: controller.state.cityController,
                  validator: (val) {},
                  isNumber: false,
                ),
                CustomTextFormField(
                  hintText: 'enter_street_name'.tr,
                  labelText: 'street'.tr,
                  icon: LineIcons.streetView,
                  textEditingController: controller.state.streetController,
                  validator: (val) {},
                  isNumber: false,
                ),
                CustomTextFormField(
                  hintText: 'enter_receiver_phone'.tr,
                  labelText: 'phone'.tr,
                  icon: LineIcons.phone,
                  textEditingController: controller.state.phoneController,
                  validator: (val) {},
                  isNumber: false,
                ),
                CustomButton(
                  onPressed: controller.addAddress(),
                  text: 'add'.tr,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
