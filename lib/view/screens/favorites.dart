import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/controller/favorites_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/view/widget/custom_search_app_bar.dart';
import 'package:nextrade/view/widget/custom_search_items_card.dart';
import 'package:nextrade/view/widget/favorites/custom_items_list.dart';

class Favorites extends GetView<FavoritesControllerImp> {
  const Favorites({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: GetBuilder<FavoritesControllerImp>(
          builder: (controller) => ListView(
            children: [
              CustomSearchAppBar(
                searchController: controller.state.searchController,
                title: 'search'.tr,
                onNotificationsPressed: () => controller.toNotificationsPage(),
                onSearchPressed: () => controller.onSearch(),
                onChanged: (value) => controller.checkSearch(value),
              ),
              const SizedBox(height: 20),
              HandlingDataView(
                statusRequest: controller.state.statusRequest,
                widget: controller.state.isSearching
                    ? ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: controller.state.searchItems.length,
                        itemBuilder: (context, index) => CustomSearchItemsCard(
                          item: controller.state.searchItems[index],
                          onIconPressed: () =>
                              controller.addOrRemoveFromFavorites(
                            controller.state.searchItems[index].id,
                          ),
                          onTap: () => controller.toItemDetailsPage(
                            controller.state.searchItems[index],
                          ),
                          icon: GetBuilder<FavoritesControllerImp>(
                            builder: (controller) => Icon(
                              controller.state.searchItems[index].favorite!
                                  ? LineIcons.heartAlt
                                  : LineIcons.heart,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      )
                    : GridView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          childAspectRatio: 0.68,
                        ),
                        itemCount: controller.state.items.length,
                        itemBuilder: (context, index) => CustomItemsList(
                          item: controller.state.items[index],
                        ),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
