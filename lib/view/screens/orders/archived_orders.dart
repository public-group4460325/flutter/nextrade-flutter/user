import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/orders/archived_orders_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/view/widget/orders/custom_orders_card.dart';

class ArchivedOrders extends GetView<ArchivedOrdersControllerImp> {
  const ArchivedOrders({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('archived_orders'.tr)),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: GetBuilder<ArchivedOrdersControllerImp>(
          builder: (controller) => HandlingDataView(
            statusRequest: controller.state.statusRequest,
            widget: ListView.builder(
              itemCount: controller.state.orders.length,
              itemBuilder: (context, index) => CustomOrdersCard(
                order: controller.state.orders[index],
                onDetailsPressed: () => controller.toOrdersDetailsPage(
                  controller.state.orders[index],
                ),
                onRatePressed: () => controller.showRating(
                  context,
                  controller.state.orders[index].id,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
