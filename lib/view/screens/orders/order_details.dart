import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/orders/order_details_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/shared/custom_button.dart';
import 'package:nextrade/view/widget/orders/custom_address_card.dart';
import 'package:nextrade/view/widget/orders/custom_map.dart';
import 'package:nextrade/view/widget/orders/custom_table.dart';
import 'package:nextrade/view/widget/orders/custom_total_price.dart';

class OrderDetails extends GetView<OrderDetailsControllerImp> {
  const OrderDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('order_details'.tr)),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: GetBuilder<OrderDetailsControllerImp>(
          builder: (controller) => HandlingDataView(
            statusRequest: controller.state.statusRequest,
            widget: ListView(
              children: [
                Card(
                  child: Column(
                    children: [
                      const CustomTable(),
                      const SizedBox(height: 10),
                      CustomTotalPrice(price: '${controller.state.totalPrice}'),
                      const SizedBox(height: 10),
                    ],
                  ),
                ),
                if (controller.state.address != null)
                  CustomAddressCard(address: controller.state.address!),
                if (controller.state.address != null) const CustomMap(),
                if (controller.state.address != null &&
                    controller.state.order.status == 3)
                  CustomButton(
                    text: 'tracking'.tr,
                    onPressed: () => controller.toTrackingPage(),
                  ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
