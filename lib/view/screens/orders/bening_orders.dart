import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/orders/pending_orders_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/view/widget/orders/custom_orders_card.dart';

class PendingOrders extends GetView<PendingOrdersControllerImp> {
  const PendingOrders({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(PendingOrdersControllerImp());
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('bending_orders'.tr)),
      body: Container(
        padding: const EdgeInsets.all(10),
        child: GetBuilder<PendingOrdersControllerImp>(
          builder: (controller) => HandlingDataView(
            statusRequest: controller.state.statusRequest,
            widget: ListView.builder(
              itemCount: controller.state.orders.length,
              itemBuilder: (context, index) => CustomOrdersCard(
                order: controller.state.orders[index],
                onDetailsPressed: () => controller.toOrdersDetailsPage(
                  controller.state.orders[index],
                ),
                onDeletePressed: () => controller.deleteOrder(
                  controller.state.orders[index].id,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
