import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:lottie/lottie.dart';
import 'package:nextrade/controller/home_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/lottie_assets.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/view/widget/custom_search_app_bar.dart';
import 'package:nextrade/view/widget/custom_search_items_card.dart';
import 'package:nextrade/view/widget/home/custom_adverts_card.dart';
import 'package:nextrade/view/widget/home/custom_categories_list.dart';
import 'package:nextrade/view/widget/home/custom_items_list.dart';
import 'package:nextrade/view/widget/home/custom_title.dart';

class Home extends GetView<HomeControllerImp> {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10),
        child: Column(
          children: [
            CustomSearchAppBar(
              searchController: controller.state.searchController,
              title: 'search'.tr,
              onNotificationsPressed: () => controller.toNotificationsPage(),
              onSearchPressed: () => controller.onSearch(),
              onChanged: (value) => controller.checkSearch(value),
            ),
            Expanded(
              child: GetBuilder<HomeControllerImp>(
                builder: (controller) => ListView(
                  physics: const BouncingScrollPhysics(),
                  children: [
                    HandlingDataView(
                      statusRequest: controller.state.statusRequest,
                      widget: controller.state.isSearching
                          ? ListView.builder(
                              shrinkWrap: true,
                              physics: const NeverScrollableScrollPhysics(),
                              itemCount: controller.state.searchItems.length,
                              itemBuilder: (context, index) =>
                                  CustomSearchItemsCard(
                                item: controller.state.searchItems[index],
                                onIconPressed: () =>
                                    controller.addOrRemoveFromFavorites(
                                  controller.state.searchItems[index].id!,
                                ),
                                onTap: () => controller.toItemDetailsPage(
                                  controller.state.searchItems[index],
                                ),
                                icon: GetBuilder<HomeControllerImp>(
                                  builder: (controller) => Icon(
                                    controller.state.searchItems[index]
                                            .favorite!
                                        ? LineIcons.heartAlt
                                        : LineIcons.heart,
                                    color: Colors.red,
                                  ),
                                ),
                              ),
                            )
                          : Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                if (controller.state.setting != null)
                                  CustomAdvertsCard(
                                    title: translateDatabase(
                                      controller.state.setting!.titleAr,
                                      controller.state.setting!.titleEn,
                                    ),
                                    body: translateDatabase(
                                      controller.state.setting!.bodyAr,
                                      controller.state.setting!.bodyEn,
                                    ),
                                  ),
                                CustomTitle(text: 'categories'.tr),
                                controller.state.categories.isEmpty
                                    ? Center(
                                        child: Lottie.asset(
                                          AppLottieAsset.noDataFound,
                                          width: Get.width / 3,
                                          height: Get.width / 3,
                                        ),
                                      )
                                    : const CustomCategoriesList(),
                                CustomTitle(text: 'products_for_you'.tr),
                                controller.state.topItems.isEmpty
                                    ? Center(
                                        child: Lottie.asset(
                                          AppLottieAsset.noDataFound,
                                          width: Get.width / 3,
                                          height: Get.width / 3,
                                        ),
                                      )
                                    : CustomItemsList(
                                        items: controller.state.topItems),
                                CustomTitle(text: 'offers'.tr),
                                controller.state.items.isEmpty
                                    ? Center(
                                        child: Lottie.asset(
                                          AppLottieAsset.noDataFound,
                                          width: Get.width / 3,
                                          height: Get.width / 3,
                                        ),
                                      )
                                    : CustomItemsList(
                                        items: controller.state.items),
                              ],
                            ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
