import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/tracking_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/view/widget/address/address_add/custom_map.dart';

class Tracking extends GetView<TrackingControllerImp> {
  const Tracking({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('tracking'.tr),
        automaticallyImplyLeading: false,
      ),
      backgroundColor: AppColor.backGroundColor,
      body: Container(
        padding: const EdgeInsets.all(10),
        child: GetBuilder<TrackingControllerImp>(
          builder: (controller) => HandlingDataView(
            statusRequest: controller.state.statusRequest,
            widget: const CustomMap(),
          ),
        ),
      ),
    );
  }
}
