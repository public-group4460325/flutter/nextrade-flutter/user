import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/notifications_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/view/widget/notifications/custom_notification_card.dart';

class Notifications extends GetView<NotificationsControllerImp> {
  const Notifications({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('notifications'.tr)),
      body: Padding(
        padding: const EdgeInsets.all(10),
        child: GetBuilder<NotificationsControllerImp>(
          builder: (controller) => HandlingDataView(
            statusRequest: controller.state.statusRequest,
            widget: ListView.builder(
              itemCount: controller.state.data.length,
              itemBuilder: (context, index) => CustomNotificationCard(
                title: controller.state.data[index].title,
                body: controller.state.data[index].body,
                time: controller.state.data[index].createdAt,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
