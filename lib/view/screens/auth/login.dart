import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/auth/login_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_request.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/core/functions/exit_alert_app.dart';
import 'package:nextrade/core/functions/validation_input.dart';
import 'package:nextrade/core/shared/custom_button.dart';
import 'package:nextrade/core/shared/custom_text_form_field.dart';
import 'package:nextrade/view/widget/auth/custom_forget_password_button.dart';
import 'package:nextrade/view/widget/auth/custom_register_or_login_button.dart';
import 'package:nextrade/view/widget/auth/custom_text_caption.dart';
import 'package:nextrade/view/widget/auth/custom_text_title.dart';
import 'package:nextrade/view/widget/auth/logo_auth.dart';

class Login extends GetView<LoginControllerImp> {
  const Login({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('login'.tr)),
      body: WillPopScope(
        onWillPop: ExitAlertApp,
        child: GetBuilder<LoginControllerImp>(
          builder: (controller) => HandlingDataRequest(
            statusRequest: controller.state.statusRequest,
            widget: Container(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 35),
              child: Form(
                key: controller.state.formState,
                child: ListView(
                  physics: const BouncingScrollPhysics(),
                  children: [
                    const LogoAuth(),
                    SizedBox(height: AppDimensions.height / 120),
                    CustomTextTitle(title: 'welcome_back'.tr),
                    SizedBox(height: AppDimensions.height / 120),
                    CustomTextCaption(caption: 'login_caption'.tr),
                    SizedBox(height: AppDimensions.height / 120),
                    CustomTextFormField(
                      isNumber: false,
                      validator: (val) {
                        return validationInput(val!, 5, 100, 'email');
                      },
                      hintText: 'email_caption'.tr,
                      labelText: 'email_label'.tr,
                      icon: Icons.email_outlined,
                      textEditingController: controller.state.emailController,
                    ),
                    CustomTextFormField(
                      onTapIcon: () => controller.showOrHidePassword(),
                      obscureText: controller.state.ishHidden,
                      isNumber: false,
                      validator: (val) {
                        return validationInput(val!, 8, 20, 'password');
                      },
                      hintText: 'password_caption'.tr,
                      labelText: 'password_label'.tr,
                      icon: controller.state.ishHidden
                          ? Icons.visibility_off
                          : Icons.visibility,
                      textEditingController:
                          controller.state.passwordController,
                    ),
                    CustomForgetPasswordButton(
                      onTap: () => controller.toForgetPasswordPage(),
                    ),
                    CustomButton(
                      text: "login".tr,
                      onPressed: () => controller.login(),
                    ),
                    SizedBox(height: AppDimensions.height / 50),
                    CustomRegisterOrLoginButton(
                      actionText: 'register_now'.tr,
                      text: "${'dont_have_an_account'.tr}   ",
                      onTap: () => controller.toSignUpPage(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
