import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/auth/forget_password/success_reset_password_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/shared/custom_button.dart';

class SuccessResetPassword extends GetView<SuccessResetPasswordControllerImp> {
  const SuccessResetPassword({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('success'.tr)),
      body: Container(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            const Center(
              child: Icon(
                Icons.check_circle_outline,
                size: 200,
                color: AppColor.primaryColor,
              ),
            ),
            Text(
              'congratulations'.tr,
              style: Theme.of(context)
                  .textTheme
                  .headlineMedium!
                  .copyWith(fontSize: 30),
            ),
            Text('successfully_registered'.tr),
            const Spacer(),
            SizedBox(
              width: double.infinity,
              child: CustomButton(
                text: 'go_to_login'.tr,
                onPressed: () {
                  controller.toLoginPage();
                },
              ),
            ),
            const SizedBox(height: 30)
          ],
        ),
      ),
    );
  }
}
