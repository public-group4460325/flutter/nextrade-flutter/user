import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/auth/forget_password/verify_code_forget_password_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_request.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/core/constants/image_assets.dart';
import 'package:nextrade/view/widget/auth/custom_resend_verification_code_button.dart';
import 'package:nextrade/view/widget/auth/custom_text_caption.dart';
import 'package:nextrade/view/widget/auth/custom_text_title.dart';
import 'package:nextrade/view/widget/auth/custom_verify_code_field.dart';

class VerifyCodeForgetPassword
    extends GetView<VerifyCodeForgetPasswordControllerImp> {
  const VerifyCodeForgetPassword({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('verification_code'.tr)),
      body: GetBuilder<VerifyCodeForgetPasswordControllerImp>(
        builder: (controller) => HandlingDataRequest(
          statusRequest: controller.state.statusRequest,
          widget: Container(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 35),
            child: ListView(
              physics: const BouncingScrollPhysics(),
              children: [
                SizedBox(height: AppDimensions.height / 40),
                Image.asset(AppImageAsset.verifyCode),
                SizedBox(height: AppDimensions.height / 20),
                CustomTextTitle(title: 'check_code'.tr),
                CustomTextCaption(
                    caption:
                        "${'check_code_caption'.tr}\n${controller.state.email}"),
                SizedBox(height: AppDimensions.height / 40),
                CustomVerifyCodeField(
                  onSubmit: (String verifyCode) {
                    controller.checkCode(verifyCode);
                  },
                ),
                SizedBox(height: AppDimensions.height / 40),
                CustomResendVerificationCodeButton(
                  onTap: () => controller.resendVerificationCode(),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
