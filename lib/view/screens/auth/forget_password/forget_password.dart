import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/auth/forget_password/forget_password_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/core/constants/image_assets.dart';
import 'package:nextrade/core/functions/validation_input.dart';
import 'package:nextrade/core/shared/custom_button.dart';
import 'package:nextrade/core/shared/custom_text_form_field.dart';
import 'package:nextrade/view/widget/auth/custom_text_caption.dart';
import 'package:nextrade/view/widget/auth/custom_text_title.dart';

class ForgetPassword extends GetView<ForgetPasswordControllerImp> {
  const ForgetPassword({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('forget_password'.tr)),
      body: GetBuilder<ForgetPasswordControllerImp>(
        builder: (controller) => HandlingDataView(
          statusRequest: controller.state.statusRequest,
          widget: Container(
            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 35),
            child: Form(
              key: controller.state.formState,
              child: ListView(
                physics: const BouncingScrollPhysics(),
                children: [
                  Image.asset(AppImageAsset.forgetPassword),
                  CustomTextTitle(title: 'check_mail'.tr),
                  CustomTextCaption(caption: 'check_email_caption'.tr),
                  SizedBox(height: AppDimensions.height / 100),
                  CustomTextFormField(
                    isNumber: false,
                    validator: (val) {
                      return validationInput(val!, 5, 100, 'email');
                    },
                    hintText: 'email_caption'.tr,
                    labelText: 'email_label'.tr,
                    icon: Icons.email_outlined,
                    textEditingController: controller.state.emailController,
                  ),
                  CustomButton(
                    text: 'check'.tr,
                    onPressed: () {
                      controller.checkMail();
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
