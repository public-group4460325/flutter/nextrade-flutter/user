import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/auth/sign_up_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_request.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/functions/exit_alert_app.dart';
import 'package:nextrade/core/functions/validation_input.dart';
import 'package:nextrade/core/shared/custom_button.dart';
import 'package:nextrade/core/shared/custom_text_form_field.dart';
import 'package:nextrade/view/widget/auth/custom_register_or_login_button.dart';
import 'package:nextrade/view/widget/auth/custom_text_caption.dart';
import 'package:nextrade/view/widget/auth/custom_text_title.dart';

class SignUp extends GetView<SignUpControllerImp> {
  const SignUp({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('sign_up'.tr)),
      body: WillPopScope(
        onWillPop: ExitAlertApp,
        child: GetBuilder<SignUpControllerImp>(
          builder: (controller) => HandlingDataRequest(
            statusRequest: controller.state.statusRequest,
            widget: Container(
              padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 35),
              child: Form(
                key: controller.state.formState,
                child: ListView(
                  physics: const BouncingScrollPhysics(),
                  children: [
                    CustomTextTitle(title: 'welcome_back'.tr),
                    CustomTextCaption(caption: 'sign_up_caption'.tr),
                    CustomTextFormField(
                      isNumber: false,
                      validator: (val) =>
                          validationInput(val!, 5, 100, 'userName'),
                      hintText: 'user_name_caption'.tr,
                      labelText: 'user_name_label'.tr,
                      icon: Icons.person_outlined,
                      textEditingController:
                          controller.state.userNameController,
                    ),
                    CustomTextFormField(
                      isNumber: false,
                      validator: (val) =>
                          validationInput(val!, 5, 100, 'email'),
                      hintText: 'email_caption'.tr,
                      labelText: 'email_label'.tr,
                      icon: Icons.email_outlined,
                      textEditingController: controller.state.emailController,
                    ),
                    CustomTextFormField(
                      onTapIcon: () => controller.showOrHidePassword(),
                      obscureText: controller.state.ishHidden,
                      isNumber: false,
                      validator: (val) =>
                          validationInput(val!, 8, 20, 'password'),
                      hintText: 'password_caption'.tr,
                      labelText: 'password_label'.tr,
                      icon: controller.state.ishHidden
                          ? Icons.visibility_off
                          : Icons.visibility,
                      textEditingController:
                          controller.state.passwordController,
                    ),
                    CustomTextFormField(
                      onTapIcon: () => controller.showOrHidePassword(),
                      obscureText: controller.state.ishHidden,
                      isNumber: false,
                      validator: (val) {
                        if (controller.state.passwordController.text !=
                            controller.state.confirmPasswordController.text) {
                          return 'not_match'.tr;
                        }
                        return validationInput(val!, 8, 20, 'password');
                      },
                      hintText: 're_password_caption'.tr,
                      labelText: 're_password_label'.tr,
                      icon: controller.state.ishHidden
                          ? Icons.visibility_off
                          : Icons.visibility,
                      textEditingController:
                          controller.state.confirmPasswordController,
                    ),
                    CustomButton(
                      text: 'sign_up'.tr,
                      onPressed: () => controller.signUp(),
                    ),
                    const SizedBox(height: 20),
                    CustomRegisterOrLoginButton(
                      actionText: 'login_now'.tr,
                      text: "${'already_have_an_account'.tr}   ",
                      onTap: () => controller.toLoginPage(),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
