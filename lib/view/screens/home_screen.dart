import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/home_screen_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/functions/exit_alert_app.dart';
import 'package:nextrade/view/widget/home_screen/custom_floating_button.dart';
import 'package:nextrade/view/widget/home_screen/custom_nav_bar.dart';

class HomeScreen extends GetView<HomeScreenControllerImp> {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(HomeScreenControllerImp());
    return WillPopScope(
      onWillPop: ExitAlertApp,
      child: GetBuilder<HomeScreenControllerImp>(
        builder: (controller) => Scaffold(
          floatingActionButton: const CustomFloatingButton(),
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          backgroundColor: AppColor.backGroundColor,
          bottomNavigationBar: const CustomNavBar(),
          body: controller.state.pagesList
              .elementAt(controller.state.currentPage),
        ),
      ),
    );
  }
}
