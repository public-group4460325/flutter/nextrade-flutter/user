import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/cart_controller/controller.dart';
import 'package:nextrade/core/classes/handling_data_view.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/view/widget/cart/custom_bottom_nav_bar.dart';
import 'package:nextrade/view/widget/cart/custom_item_card.dart';
import 'package:nextrade/view/widget/cart/custom_top_title.dart';

class Cart extends GetView<CartControllerImp> {
  const Cart({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      appBar: AppBar(title: Text('cart'.tr)),
      bottomNavigationBar: GetBuilder<CartControllerImp>(
        builder: (controller) => CustomBottomNavBar(
          couponName: controller.state.coupon.code,
          price: controller.state.price,
          discount: controller.state.coupon.discount!,
          shipping: controller.state.shipping,
          total: controller.state.price -
              (controller.state.price *
                  controller.state.coupon.discount! /
                  100) +
              controller.state.shipping,
          onPlaceOrder: () => controller.toCheckOutPage(),
        ),
      ),
      body: ListView(
        children: [
          const SizedBox(height: 10),
          GetBuilder<CartControllerImp>(
            builder: (controller) =>
                CustomTopTitle(count: controller.state.items.length),
          ),
          GetBuilder<CartControllerImp>(
            builder: (controller) => HandlingDataView(
              statusRequest: controller.state.statusRequest,
              widget: ListView.builder(
                shrinkWrap: true,
                physics: const NeverScrollableScrollPhysics(),
                itemCount: controller.state.items.length,
                itemBuilder: (context, index) =>
                    CustomItemCard(item: controller.state.items[index]),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
