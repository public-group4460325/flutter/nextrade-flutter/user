import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/controller/settings_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/core/constants/image_assets.dart';

class Settings extends GetView<SettingsControllerImp> {
  const Settings({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10),
      child: ListView(
        children: [
          Stack(
            clipBehavior: Clip.none,
            alignment: Alignment.center,
            children: [
              Container(
                height: AppDimensions.width / 3,
                color: AppColor.primaryColor,
              ),
              Positioned(
                top: AppDimensions.width / 4.25,
                child: Container(
                  padding: const EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    color: AppColor.backGroundColor,
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: const CircleAvatar(
                    backgroundColor: AppColor.backGroundColor,
                    radius: 40,
                    backgroundImage: AssetImage(AppImageAsset.profile),
                  ),
                ),
              ),
            ],
          ),
          const SizedBox(
            height: 70,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Card(
              child: Column(
                children: [
                  ListTile(
                    title: Text(
                      'notifications'.tr,
                      style: const TextStyle(fontSize: 20),
                    ),
                    trailing: Switch(
                      onChanged: (value) {},
                      value: true,
                    ),
                  ),
                  const Divider(),
                  ListTile(
                    title: Text(
                      'bending_orders'.tr,
                      style: const TextStyle(fontSize: 20),
                    ),
                    trailing: const Icon(
                      LineIcons.shoppingBag,
                      size: 30,
                    ),
                    onTap: () => controller.toOrdersPage(),
                  ),
                  const Divider(),
                  ListTile(
                    title: Text(
                      'archived_orders'.tr,
                      style: const TextStyle(fontSize: 20),
                    ),
                    trailing: const Icon(
                      LineIcons.archive,
                      size: 30,
                    ),
                    onTap: () => controller.toArchivedOrdersPage(),
                  ),
                  const Divider(),
                  ListTile(
                    title: Text(
                      'addresses'.tr,
                      style: const TextStyle(fontSize: 20),
                    ),
                    trailing: const Icon(
                      LineIcons.map,
                      size: 30,
                    ),
                    onTap: () => controller.toAddressPage(),
                  ),
                  const Divider(),
                  ListTile(
                    title: Text(
                      'about_us'.tr,
                      style: const TextStyle(fontSize: 20),
                    ),
                    trailing: const Icon(
                      LineIcons.info,
                      size: 30,
                    ),
                    onTap: () {},
                  ),
                  const Divider(),
                  ListTile(
                    title: Text(
                      'contact_us'.tr,
                      style: const TextStyle(fontSize: 20),
                    ),
                    trailing: const Icon(
                      LineIcons.phone,
                      size: 30,
                    ),
                    onTap: () => controller.contactUs(),
                  ),
                  const Divider(),
                  ListTile(
                    title: Text(
                      'logout'.tr,
                      style: const TextStyle(fontSize: 20),
                    ),
                    trailing: const Icon(
                      LineIcons.powerOff,
                      size: 30,
                    ),
                    onTap: () {
                      controller.logOut();
                    },
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
