import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/item_details_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/view/widget/item_details/custom_bottom_nav_bar.dart';
import 'package:nextrade/view/widget/item_details/custom_price_and_count.dart';
import 'package:nextrade/view/widget/item_details/top_page_item_details.dart';

class ItemDetails extends GetView<ItemDetailsControllerImp> {
  const ItemDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backGroundColor,
      bottomNavigationBar: const CustomBottomNavBar(),
      body: ListView(
        children: [
          const TopPageItemDetails(),
          const SizedBox(height: 100),
          Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  translateDatabase(
                    controller.state.item.nameAr,
                    controller.state.item.nameEn,
                  ),
                  style: Theme.of(context)
                      .textTheme
                      .headlineLarge
                      ?.copyWith(color: AppColor.primaryColor),
                ),
                const SizedBox(height: 10),
                GetBuilder<ItemDetailsControllerImp>(
                  builder: (controller) => CustomPriceAndCount(
                    onAddPressed: () => controller.increaseCount(),
                    onRemovePressed: () => controller.decreaseCount(),
                    price:
                        ' ${controller.state.item.price! - (controller.state.item.price! * controller.state.item.discount! / 100)} \$',
                    count: '${controller.state.count}',
                  ),
                ),
                const SizedBox(height: 10),
                Text(
                  translateDatabase(
                    controller.state.item.deskAr,
                    controller.state.item.deskEn,
                  ),
                  style: Theme.of(context).textTheme.bodyLarge,
                ),
                const SizedBox(height: 10),
                // Text(
                //   'Color',
                //   style: Theme.of(context).textTheme.headlineLarge,
                // ),
                // const SizedBox(height: 10),
                // const CustomSubItemsList(),
              ],
            ),
          )
        ],
      ),
    );
  }
}
