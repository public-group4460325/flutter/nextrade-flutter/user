import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/color.dart';


class CustomChooseDeliveryType extends StatelessWidget {
  final String title;
  final String imageUrl;
  final bool isActive;
  final void Function() onTap;
  const CustomChooseDeliveryType({
    super.key,
    required this.title,
    required this.imageUrl,
    required this.isActive,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 5),
        height: 120,
        width: 120,
        decoration: BoxDecoration(
          border: Border.all(color: AppColor.primaryColor),
          color: isActive ? AppColor.primaryColor : null,
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(
              imageUrl,
              width: 80,
              color: isActive ? Colors.white : null,
            ),
            Text(
              title,
              style: TextStyle(
                color: isActive ? Colors.white : AppColor.primaryColor,
                fontWeight: FontWeight.bold,
              ),
            )
          ],
        ),
      ),
    );
  }
}
