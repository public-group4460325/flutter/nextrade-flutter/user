import 'package:flutter/material.dart';
import 'package:nextrade/data/models/radio_model.dart';

class CustomChoosePaymentMethodRadio extends StatelessWidget {
  final int selectedValue;
  final String title;
  final RadioModel radio;
  final void Function(int?) onChanged;
  const CustomChoosePaymentMethodRadio({
    super.key,
    required this.title,
    required this.radio,
    required this.onChanged,
    required this.selectedValue,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width / 2.3,
      child: RadioListTile(
        title: Text(title),
        // title: Text('${controller.state.radioItems[index]['text']}'),
        groupValue: selectedValue,
        // groupValue: controller.state.selectedPaymentValue,
        value: radio.value,
        // value: controller.state.radioItems[index]['radioModel'].value,
        onChanged: onChanged,
        // onChanged: (value) => controller.changePaymentMethod(value),
        controlAffinity: ListTileControlAffinity.leading,
      ),
    );
  }
}
