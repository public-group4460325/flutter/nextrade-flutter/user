import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/data/models/address_model.dart';

class CustomChooseDeliveryAddress extends StatelessWidget {
  final Address address;
  final bool isActive;
  final void Function() onTap;
  const CustomChooseDeliveryAddress({
    super.key,
    required this.address,
    required this.isActive,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Card(
        color: isActive ? AppColor.primaryColor : null,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        child: ListTile(
          title: Text(
            address.name!,
            style: TextStyle(
              color: isActive ? Colors.white : null,
              fontWeight: FontWeight.bold,
            ),
          ),
          subtitle: Text(
            '${address.city} - ${address.street}',
            style: TextStyle(
              color: isActive ? Colors.white : null,
            ),
          ),
        ),
      ),
    );
  }
}
