import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/home_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';

class CustomAdvertsCard extends GetView<HomeControllerImp> {
  final String title;
  final String body;
  const CustomAdvertsCard({
    super.key,
    required this.title,
    required this.body,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 15),
      child: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            height: 150,
            decoration: BoxDecoration(
              color: AppColor.primaryColor,
              borderRadius: BorderRadius.circular(20),
            ),
            child: ListTile(
              title: Text(
                title,
                textAlign:
                    controller.state.lang == 'ar' ? TextAlign.right : null,
                style: const TextStyle(color: Colors.white, fontSize: 20),
              ),
              subtitle: Text(
                body,
                textAlign:
                    controller.state.lang == 'ar' ? TextAlign.right : null,
                style: const TextStyle(color: Colors.white, fontSize: 30),
              ),
            ),
          ),
          Positioned(
            top: -20,
            right: controller.state.lang == 'en' ? -20 : null,
            left: controller.state.lang == 'ar' ? -20 : null,
            child: Container(
              height: 160,
              width: 160,
              decoration: BoxDecoration(
                color: AppColor.secondaryColor,
                borderRadius: BorderRadius.circular(160),
              ),
            ),
          )
        ],
      ),
    );
  }
}
