import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:lottie/lottie.dart';
import 'package:nextrade/controller/home_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/link_server.dart';
import 'package:nextrade/core/constants/lottie_assets.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/data/models/item_model.dart';

class CustomItemsList extends GetView<HomeControllerImp> {
  final List items;
  const CustomItemsList({super.key, required this.items});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 140,
      child: ListView.builder(
        physics: const BouncingScrollPhysics(),
        itemCount: items.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => Items(
          item: items[index],
        ),
      ),
    );
  }
}

class Items extends GetView<HomeControllerImp> {
  final Item item;
  const Items({super.key, required this.item});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => controller.toItemDetailsPage(item),
      child: Stack(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 20,
              vertical: 10,
            ),
            margin: const EdgeInsets.symmetric(
              horizontal: 10,
            ),
            child: CachedNetworkImage(
              imageUrl: '${AppLinkServer.storage}/${item.image}',
              placeholder: (context, url) => Center(
                child: Lottie.asset(
                  AppLottieAsset.loading,
                ),
              ),
              height: 100,
              width: 150,
              fit: BoxFit.fill,
            ),
          ),
          Container(
            decoration: BoxDecoration(
              color: AppColor.black.withOpacity(0.3),
              borderRadius: BorderRadius.circular(20),
            ),
            height: 120,
            width: 200,
          ),
          Positioned(
            left: controller.state.lang == 'en' ? 30 : null,
            right: controller.state.lang == 'ar' ? 30 : null,
            child: Text(
              '${translateDatabase(item.nameAr, item.nameEn)}',
              textAlign: controller.state.lang == 'ar' ? TextAlign.right : null,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
            ),
          )
        ],
      ),
    );
  }
}
