import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/home_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/link_server.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/data/models/category_model.dart';

class CustomCategoriesList extends GetView<HomeControllerImp> {
  const CustomCategoriesList({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      child: ListView.separated(
        physics: const BouncingScrollPhysics(),
        separatorBuilder: (context, index) => const SizedBox(width: 10),
        itemCount: controller.state.categories.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => Categories(
          index: index,
          category: controller.state.categories[index],
        ),
      ),
    );
  }
}

class Categories extends GetView<HomeControllerImp> {
  final Category category;
  final int index;
  const Categories({super.key, required this.category, required this.index});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => controller.toItemsPage(controller.state.categories, index),
      child: Column(
        children: [
          Container(
            decoration: BoxDecoration(
              color: AppColor.primaryColor,
              borderRadius: BorderRadius.circular(20),
            ),
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
            ),
            height: 70,
            width: 70,
            child: SvgPicture.network(
              "${AppLinkServer.storage}/${category.image}",
              color: Colors.white,
            ),
          ),
          Text(
            '${translateDatabase(category.nameAr, category.nameEn)}',
            style: const TextStyle(
              fontSize: 13,
              color: AppColor.black,
            ),
          ),
        ],
      ),
    );
  }
}
