import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/link_server.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/data/models/item_model.dart';

class CustomSearchItemsCard extends StatelessWidget {
  final Item item;
  final void Function() onIconPressed;
  final void Function() onTap;
  final Widget icon;
  const CustomSearchItemsCard({
    super.key,
    required this.item,
    required this.onIconPressed,
    required this.onTap,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      child: InkWell(
        onTap: () => onTap,
        child: Card(
          child: Padding(
            padding: const EdgeInsets.all(10),
            child: Row(
              children: [
                Expanded(
                  child: CachedNetworkImage(
                      imageUrl: '${AppLinkServer.storage}/${item.image!}'),
                ),
                Expanded(
                  flex: 2,
                  child: ListTile(
                    title: Text(
                      translateDatabase(
                        item.nameAr,
                        item.nameEn,
                      ),
                    ),
                    subtitle: Text(
                      '${item.price} \$',
                      style: const TextStyle(
                        fontFamily: 'sans',
                        color: AppColor.primaryColor,
                        fontSize: 20,
                      ),
                    ),
                    trailing: IconButton(
                      onPressed: onIconPressed,
                      icon: icon,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
