import 'package:flutter/material.dart';
import 'package:jiffy/jiffy.dart';

class CustomNotificationCard extends StatelessWidget {
  final String title;
  final String body;
  final String time;
  const CustomNotificationCard({
    super.key,
    required this.title,
    required this.body,
    required this.time,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(vertical: 5),
      child: ListTile(
        title: Text(title),
        subtitle: Text(body),
        trailing: Text(
          Jiffy.parse(time).fromNow(),
          style: const TextStyle(
            // fontSize: 15,
            fontWeight: FontWeight.bold,
            // color: AppColor.primaryColor,
          ),
        ),
      ),
    );
  }
}
