import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/onboarding_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';

class CustomContinueButton extends GetView<OnBoardingControllerImp> {
  const CustomContinueButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: AppDimensions.height / 20),
      height: AppDimensions.height / 20,
      padding: EdgeInsets.symmetric(horizontal: AppDimensions.width / 4),
      child: MaterialButton(
        shape: RoundedRectangleBorder(
          side: const BorderSide(
            color: AppColor.primaryColor,
            width: 3,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
        textColor: AppColor.primaryColor,
        color: AppColor.backGroundColor,
        minWidth: double.infinity,
        onPressed: () {
          controller.next();
        },
        child: Text(
          'continue'.tr,
          style: const TextStyle(color: AppColor.primaryColor),
        ),
      ),
    );
  }
}
