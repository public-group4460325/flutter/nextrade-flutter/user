import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/onboarding_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/data/data_source/static/static.dart';

class CustomDotController extends StatelessWidget {
  const CustomDotController({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<OnBoardingControllerImp>(
      init: OnBoardingControllerImp(),
      builder: (controller) => Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          ...List.generate(
            onBoardingList.length,
            (index) => AnimatedContainer(
              margin: EdgeInsets.only(right: AppDimensions.width / 70),
              duration: const Duration(milliseconds: 500),
              width: controller.state.currentPage == index
                  ? AppDimensions.width / 15
                  : AppDimensions.width / 70,
              height: AppDimensions.height / 125,
              decoration: BoxDecoration(
                color: controller.state.currentPage == index
                    ? AppColor.primaryColor
                    : AppColor.grey,
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
