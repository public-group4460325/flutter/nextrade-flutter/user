import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/onboarding_controller/controller.dart';
import 'package:nextrade/core/constants/dimensions.dart';

class CustomSkipButton extends GetView<OnBoardingControllerImp> {
  const CustomSkipButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: AppDimensions.height / 50),
      child: InkWell(
        child: Text('skip'.tr),
        onTap: () {
          controller.skip();
        },
      ),
    );
  }
}
