import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/onboarding_controller/controller.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/data/data_source/static/static.dart';

class CustomSlider extends GetView<OnBoardingControllerImp> {
  const CustomSlider({super.key});

  @override
  Widget build(BuildContext context) {
    return PageView.builder(
      physics: const BouncingScrollPhysics(),
      controller: controller.state.pageController,
      onPageChanged: (val) {
        controller.onPageChanged(val);
      },
      itemCount: onBoardingList.length,
      itemBuilder: (context, i) => Column(
        children: [
          Text(
            onBoardingList[i].title!,
            style: Theme.of(context).textTheme.headlineMedium,
          ),
          SizedBox(height: AppDimensions.height / 20),
          Image.asset(
            onBoardingList[i].imageUrl!,
            width: AppDimensions.width / 1.5,
            fit: BoxFit.fill,
          ),
          SizedBox(height: AppDimensions.height / 10),
          Container(
            width: double.infinity,
            alignment: Alignment.center,
            child: Text(
              onBoardingList[i].body!,
              textAlign: TextAlign.center,
              style: Theme.of(context).textTheme.bodyLarge,
            ),
          )
        ],
      ),
    );
  }
}
