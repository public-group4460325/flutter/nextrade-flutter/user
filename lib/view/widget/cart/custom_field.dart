import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/color.dart';

class Field extends StatelessWidget {
  final String title;
  final String value;
  const Field({
    super.key,
    required this.title,
    required this.value,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            title,
            style: TextStyle(
              fontSize: 16,
              color: title == 'total_price'.tr ? AppColor.primaryColor : null,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Text(
            value,
            style: TextStyle(
              fontSize: 16,
              fontFamily: 'sans',
              color: title == 'total_price'.tr ? AppColor.primaryColor : null,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
