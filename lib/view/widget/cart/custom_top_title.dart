import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/color.dart';

class CustomTopTitle extends StatelessWidget {
  final int count;
  const CustomTopTitle({super.key, required this.count});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20),
      padding: const EdgeInsets.only(bottom: 5),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: AppColor.primaryColor,
      ),
      child: Text(
        '${'you_have'.tr} $count ${'items_in_your_cart'.tr}',
        textAlign: TextAlign.center,
        style: const TextStyle(color: Colors.white, fontSize: 17),
      ),
    );
  }
}
