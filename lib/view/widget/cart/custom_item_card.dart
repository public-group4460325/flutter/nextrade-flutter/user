import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/controller/cart_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/core/constants/link_server.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/data/models/cart_item_model.dart';

class CustomItemCard extends GetView<CartControllerImp> {
  final CartItem item;
  const CustomItemCard({
    super.key,
    required this.item,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Card(
        child: Container(
          child: Column(
            children: [
              Row(
                children: [
                  Expanded(
                    flex: 2,
                    child: CachedNetworkImage(
                      imageUrl: '${AppLinkServer.storage}/${item.image}',
                      height: 100,
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: ListTile(
                      title: Text(
                        translateDatabase(item.nameAr, item.nameEn),
                        style: const TextStyle(
                          fontSize: 15,
                        ),
                      ),
                      subtitle: Text(
                        '${item.price! - (item.price! * item.discount! / 100)} \$',
                        style: const TextStyle(
                          fontFamily: 'sans',
                          color: AppColor.primaryColor,
                          fontSize: 20,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    child: Column(
                      children: [
                        IconButton(
                          onPressed: () => controller.increaseCount(item),
                          icon: const Icon(LineIcons.plus),
                        ),
                        GetBuilder<CartControllerImp>(
                          builder: (controller) => Text(
                            '${item.count}',
                            style: const TextStyle(
                              fontFamily: 'sans',
                            ),
                          ),
                        ),
                        IconButton(
                          onPressed: () => controller.decreaseCount(item),
                          icon: const Icon(LineIcons.minus),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    CustomButton(
                      color: Colors.red,
                      text: 'delete'.tr,
                      onPressed: () => controller.delete(item),
                    ),
                    CustomButton(
                      color: AppColor.primaryColor,
                      text: 'save'.tr,
                      onPressed: () => controller.save(item),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  final void Function() onPressed;
  final String text;
  final Color color;
  const CustomButton({
    super.key,
    required this.onPressed,
    required this.text,
    required this.color,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: AppDimensions.width / 3,
      height: AppDimensions.height / 20,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      onPressed: onPressed,
      color: color,
      child: Text(
        text,
        style: const TextStyle(
          color: Colors.white,
          fontWeight: FontWeight.bold,
          fontSize: 15,
        ),
      ),
    );
  }
}
