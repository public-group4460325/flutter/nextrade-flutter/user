import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomTextForFieldCoupon extends StatelessWidget {
  final TextEditingController couponController;
  final String? Function(String?) validator;
  const CustomTextForFieldCoupon(
      {super.key, required this.couponController, required this.validator});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration: InputDecoration(
        isDense: true,
        contentPadding:
            const EdgeInsets.symmetric(vertical: 12, horizontal: 10),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        hintText: 'enter_coupon_code'.tr,
      ),
      controller: couponController,
    );
  }
}
