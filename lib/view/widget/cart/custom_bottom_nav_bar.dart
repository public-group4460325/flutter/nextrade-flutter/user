import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/color.dart';

import 'custom_bottom_nav_bar_button.dart';
import 'custom_coupon_input.dart';
import 'custom_field.dart';

class CustomBottomNavBar extends StatelessWidget {
  final double price;
  final double shipping;
  final int discount;
  final double total;
  final String? couponName;
  final void Function() onPlaceOrder;
  const CustomBottomNavBar({
    super.key,
    required this.price,
    required this.discount,
    required this.shipping,
    required this.total,
    required this.couponName,
    required this.onPlaceOrder,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          couponName == null
              ? const CustomCouponInput()
              : Container(
                  padding: const EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    "${'using_coupon'.tr}: $couponName",
                    style: const TextStyle(
                      fontSize: 25,
                      color: AppColor.primaryColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
          Container(
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.symmetric(horizontal: 5),
            decoration: BoxDecoration(
              border: Border.all(color: AppColor.primaryColor, width: 2),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              children: [
                Field(title: 'price'.tr, value: '$price'),
                Field(title: 'discount'.tr, value: '$discount'),
                Field(title: 'shipping'.tr, value: '$shipping'),
                const Divider(color: Colors.black),
                Field(title: 'total_price'.tr, value: '$total'),
                const SizedBox(height: 10),
              ],
            ),
          ),
          CustomBottomNavBarButton(
            onPressed: onPlaceOrder,
          ),
        ],
      ),
    );
  }
}
