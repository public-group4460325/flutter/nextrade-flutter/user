import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/cart_controller/controller.dart';
import 'package:nextrade/view/widget/cart/custom_text_form_field_coupon.dart';
import 'package:nextrade/view/widget/cart/custom_coupon_button.dart';

class CustomCouponInput extends GetView<CartControllerImp> {
  const CustomCouponInput({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: CustomTextForFieldCoupon(
              validator: (val) {},
              couponController: controller.state.couponController,
            ),
          ),
          Expanded(
            child: CustomCouponButton(
              onPressed: () => controller.checkCoupon(),
            ),
          ),
        ],
      ),
    );
  }
}
