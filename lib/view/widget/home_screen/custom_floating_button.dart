import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/controller/home_screen_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';

class CustomFloatingButton extends GetView<HomeScreenControllerImp> {
  const CustomFloatingButton({super.key});

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      backgroundColor: AppColor.primaryColor,
      onPressed: () => controller.toCartPage(),
      child: const Icon(LineIcons.shoppingCart),
    );
  }
}
