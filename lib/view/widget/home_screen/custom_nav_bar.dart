import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/home_screen_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'custom_nav_bar_button.dart';

class CustomNavBar extends GetView<HomeScreenControllerImp> {
  const CustomNavBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColor.backGroundColor,
      child: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        notchMargin: 10,
        child: Container(
          color: AppColor.backGroundColor,
          child: Row(
            children: [
              ...List.generate(
                controller.state.pagesList.length + 1,
                (index) {
                  int i = index > 2 ? index - 1 : index;
                  return index == 2
                      ? const Spacer()
                      : GetBuilder<HomeScreenControllerImp>(
                          builder: (controller) => CustomNavBarButton(
                            title: controller.state.bottomNavBarActions[i]
                                ['title'],
                            icon: controller.state.bottomNavBarActions[i]
                                ['icon'],
                            onPressed: () {
                              controller.changePage(i);
                            },
                            isActive: controller.state.currentPage == i,
                          ),
                        );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
