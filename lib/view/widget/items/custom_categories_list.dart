import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/items_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/data/models/category_model.dart';

class CustomCategoriesList extends GetView<ItemsControllerImp> {
  const CustomCategoriesList({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      child: ListView.separated(
        separatorBuilder: (context, index) => const SizedBox(width: 10),
        itemCount: controller.state.categories.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => Categories(
          index: index,
          category: controller.state.categories[index],
        ),
      ),
    );
  }
}

class Categories extends GetView<ItemsControllerImp> {
  final Category category;
  final int index;
  const Categories({super.key, required this.category, required this.index});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => controller.changeCategory(index),
      child: Column(
        children: [
          const SizedBox(height: 20),
          GetBuilder<ItemsControllerImp>(
            builder: (controller) => Container(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              height: 40,
              decoration: controller.state.selectedCategory == index
                  ? const BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: AppColor.primaryColor,
                          width: 4,
                        ),
                      ),
                    )
                  : null,
              child: Text(
                '${translateDatabase(category.nameAr, category.nameEn)}',
                style: TextStyle(
                  fontSize: 20,
                  color: controller.state.selectedCategory == index
                      ? AppColor.primaryColor
                      : AppColor.black,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
