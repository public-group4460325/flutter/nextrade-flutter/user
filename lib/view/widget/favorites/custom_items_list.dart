import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/controller/favorites_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/image_assets.dart';
import 'package:nextrade/core/constants/link_server.dart';
import 'package:nextrade/core/functions/translate_database.dart';
import 'package:nextrade/data/models/item_model.dart';

class CustomItemsList extends GetView<FavoritesControllerImp> {
  final Item item;
  const CustomItemsList({
    super.key,
    required this.item,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        controller.toItemDetailsPage(item);
      },
      child: Stack(
        children: [
          Card(
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Hero(
                    tag: '${item.id}',
                    child: CachedNetworkImage(
                      imageUrl: '${AppLinkServer.storage}/${item.image}',
                      height: 100,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Text(
                    '${translateDatabase(item.nameAr, item.nameEn)}',
                    style: const TextStyle(
                      color: AppColor.black,
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          Text(
                            ' ${item.price} \$',
                            style: TextStyle(
                              color: Colors.red,
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'sans',
                              decoration: item.discount != 0
                                  ? TextDecoration.lineThrough
                                  : null,
                            ),
                          ),
                          if (item.discount != 0)
                            Text(
                              '${item.price! - (item.price! * item.discount! / 100)} \$',
                              style: const TextStyle(
                                color: Colors.red,
                                fontSize: 20,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'sans',
                              ),
                            ),
                        ],
                      ),
                      IconButton(
                        onPressed: () {
                          controller.addOrRemoveFromFavorites(item.id!);
                        },
                        icon: const Icon(
                          LineIcons.minus,
                          color: Colors.red,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          if (item.discount != 0)
            Image.asset(
              AppImageAsset.discount,
              width: 60,
            ),
        ],
      ),
    );
  }
}
