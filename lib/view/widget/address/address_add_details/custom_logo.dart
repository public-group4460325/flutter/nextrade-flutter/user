import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/core/constants/image_assets.dart';

class CustomLogo extends StatelessWidget {
  const CustomLogo({super.key});

  @override
  Widget build(BuildContext context) {
    return Image.asset(
      AppImageAsset.location,
      height: AppDimensions.height / 3,
    );
  }
}
