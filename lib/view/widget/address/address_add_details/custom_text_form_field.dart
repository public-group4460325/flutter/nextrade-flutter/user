import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';


class CustomTextFormField extends StatelessWidget {
  final String hintText;
  final String labelText;
  final IconData icon;
  final TextEditingController textEditingController;
  final String? Function(String?) validator;
  final bool isNumber;
  final bool? obscureText;
  final void Function()? onTapIcon;

  const CustomTextFormField({
    super.key,
    required this.hintText,
    required this.labelText,
    required this.icon,
    required this.textEditingController,
    required this.validator,
    required this.isNumber,
    this.obscureText = false,
    this.onTapIcon,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
        bottom: AppDimensions.height / 40,
        top: AppDimensions.height / 80,
      ),
      child: TextFormField(
        obscureText: obscureText!,
        keyboardType: isNumber
            ? const TextInputType.numberWithOptions()
            : TextInputType.text,
        validator: validator,
        controller: textEditingController,
        decoration: InputDecoration(
          suffixIcon: InkWell(
            onTap: onTapIcon,
            child: Icon(icon),
          ),
          floatingLabelBehavior: FloatingLabelBehavior.always,
          label: Container(
            margin: EdgeInsets.symmetric(horizontal: AppDimensions.width / 30),
            child: Text(labelText),
          ),
          hintText: hintText,
          contentPadding: EdgeInsets.symmetric(
            vertical: AppDimensions.height / 70,
            horizontal: AppDimensions.width / 20,
          ),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
          ),
          filled: true,
          fillColor: AppColor.textFormColor,
        ),
      ),
    );
  }
}
