import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';

class CustomDeleteButton extends StatelessWidget {
  final void Function() onPressed;
  const CustomDeleteButton({super.key, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        LineIcons.trash,
        color: Colors.red,
        size: 30,
      ),
      onPressed: onPressed,
    );
  }
}
