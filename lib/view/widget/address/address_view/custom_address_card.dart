import 'package:flutter/material.dart';
import 'package:nextrade/data/models/address_model.dart';

import 'custom_delete_button.dart';

class CustomAddressCard extends StatelessWidget {
  final void Function() onDelete;
  final Address address;
  const CustomAddressCard(
      {super.key, required this.address, required this.onDelete});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Container(
        padding: const EdgeInsets.all(10),
        child: ListTile(
          title: Text(
            address.name!,
            style: const TextStyle(fontSize: 25),
          ),
          subtitle: Text(
            "${address.city} - ${address.street}",
            style: const TextStyle(fontSize: 20),
          ),
          trailing: CustomDeleteButton(onPressed: onDelete),
        ),
      ),
    );
  }
}
