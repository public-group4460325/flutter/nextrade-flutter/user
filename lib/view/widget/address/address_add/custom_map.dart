import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:nextrade/controller/address/address_add_controller/controller.dart';


class CustomMap extends GetView<AddressAddControllerImp> {
  const CustomMap({super.key});

  @override
  Widget build(BuildContext context) {
    return GoogleMap(
      onTap: (latLng) => controller.addMarkers(latLng),
      markers: controller.state.markers.toSet(),
      mapType: MapType.normal,
      initialCameraPosition: controller.state.kGooglePlex,
      onMapCreated: (GoogleMapController googleMapController) {
        controller.state.controller.complete(googleMapController);
      },
    );
  }
}
