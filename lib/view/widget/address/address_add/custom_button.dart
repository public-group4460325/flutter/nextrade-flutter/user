import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/address/address_add_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';

class CustomButton extends GetView<AddressAddControllerImp> {
  const CustomButton({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: AppDimensions.height / 35),
      child: MaterialButton(
        minWidth: 200,
        padding: EdgeInsets.symmetric(vertical: AppDimensions.height / 60),
        shape: RoundedRectangleBorder(
          side: const BorderSide(
            color: AppColor.primaryColor,
            width: 3,
          ),
          borderRadius: BorderRadius.circular(15),
        ),
        textColor: AppColor.primaryColor,
        color: AppColor.backGroundColor,
        onPressed: () => controller.toAddressAddPartTwo(),
        child: Text(
          'continue'.tr,
          style: const TextStyle(
            fontSize: 18,
          ),
        ),
      ),
    );
  }
}
