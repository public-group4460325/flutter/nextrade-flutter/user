import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/dimensions.dart';

class CustomTextCaption extends StatelessWidget {
  final String caption;

  const CustomTextCaption({super.key, required this.caption});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: AppDimensions.width / 50),
      padding: EdgeInsets.only(bottom: AppDimensions.height / 50),
      child: Text(
        caption,
        style: Theme.of(context).textTheme.bodyLarge,
        textAlign: TextAlign.center,
      ),
    );
  }
}
