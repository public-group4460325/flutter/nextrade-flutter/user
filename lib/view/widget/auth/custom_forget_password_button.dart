import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/color.dart';

class CustomForgetPasswordButton extends StatelessWidget {
  final void Function() onTap;
  const CustomForgetPasswordButton({super.key, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 10),
      child: InkWell(
        onTap: onTap,
        child: Text(
          'forget_password'.tr,
          style: const TextStyle(
            color: AppColor.primaryColor,
            fontFamily: 'PlayfairDisplay',
          ),
          textAlign: TextAlign.end,
        ),
      ),
    );
  }
}
