import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/color.dart';

class CustomRegisterOrLoginButton extends StatelessWidget {
  final String text;
  final String actionText;

  final void Function() onTap;

  const CustomRegisterOrLoginButton(
      {super.key,
      required this.text,
      required this.onTap,
      required this.actionText});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(text),
        InkWell(
          onTap: onTap,
          child: Text(
            actionText,
            style: const TextStyle(
              color: AppColor.primaryColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }
}
