import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/auth/verify_code_sign_up_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';

class CustomResendVerificationCodeButton
    extends GetView<VerifyCodeSignUpControllerImp> {
  final void Function() onTap;
  const CustomResendVerificationCodeButton({super.key, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: InkWell(
        onTap: onTap,
        child: Text(
          'resend_verification_code',
          style: TextStyle(
            color: AppColor.primaryColor,
            fontSize: AppDimensions.height / 50,
          ),
        ),
      ),
    );
  }
}
