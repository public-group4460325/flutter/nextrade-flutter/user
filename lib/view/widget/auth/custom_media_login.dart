import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/dimensions.dart';

class CustomMediaLogin extends StatelessWidget {
  final String imageUrl;
  final void Function() onTap;
  const CustomMediaLogin({
    super.key,
    required this.imageUrl,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text('login_with'.tr),
        SizedBox(height: AppDimensions.height / 50),
        Container(
          padding: EdgeInsets.symmetric(
            vertical: AppDimensions.height / 80,
            horizontal: AppDimensions.width / 5,
          ),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(70),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 5,
                blurRadius: 7,
                offset: const Offset(0, 3), // changes position of shadow
              ),
            ],
          ),
          child: InkWell(
            onTap: () {},
            child: Image.asset(
              imageUrl,
              width: 50,
            ),
          ),
        ),
        const SizedBox(height: 10),
      ],
    );
  }
}
