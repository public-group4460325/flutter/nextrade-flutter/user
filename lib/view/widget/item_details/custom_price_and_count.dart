import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:nextrade/core/constants/color.dart';

class CustomPriceAndCount extends StatelessWidget {
  final void Function() onAddPressed;
  final void Function() onRemovePressed;
  final String price;
  final String count;

  const CustomPriceAndCount({
    super.key,
    required this.onAddPressed,
    required this.onRemovePressed,
    required this.price,
    required this.count,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Row(
          children: [
            IconButton(
              onPressed: onRemovePressed,
              icon: const Icon(LineIcons.minus),
            ),
            Container(
              width: 50,
              alignment: Alignment.center,
              padding: const EdgeInsets.all(3),
              decoration: BoxDecoration(
                border: Border.all(
                  color: AppColor.black,
                ),
              ),
              child: Text(
                count,
                style: const TextStyle(
                  fontFamily: 'sans',
                  fontSize: 20,
                ),
              ),
            ),
            IconButton(
              onPressed: onAddPressed,
              icon: const Icon(LineIcons.plus),
            ),
          ],
        ),
        const Spacer(),
        Text(
          '$price\$',
          style: const TextStyle(
            color: AppColor.primaryColor,
            fontSize: 30,
            fontFamily: 'sans',
          ),
        )
      ],
    );
  }
}
