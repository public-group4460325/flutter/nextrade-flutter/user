import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/item_details_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';

class CustomBottomNavBar extends GetView<ItemDetailsControllerImp> {
  const CustomBottomNavBar({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(10),
      height: 60,
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          CustomButton(
            onPressed: () => controller.addToCart(),
            text: 'add_to_cart'.tr,
          ),
          CustomButton(
            onPressed: () => controller.removeFromCart(),
            text: 'remove_from_cart'.tr,
          ),
        ],
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  final void Function() onPressed;
  final String text;
  const CustomButton({
    super.key,
    required this.onPressed,
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      minWidth: AppDimensions.width / 2.3,
      height: AppDimensions.height / 17,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
        side: const BorderSide(
          color: AppColor.primaryColor,
          width: 3,
        ),
      ),
      onPressed: onPressed,
      color: AppColor.backGroundColor,
      child: Text(
        text,
        style: const TextStyle(
          color: AppColor.primaryColor,
          fontWeight: FontWeight.bold,
          fontSize: 15,
        ),
      ),
    );
  }
}
