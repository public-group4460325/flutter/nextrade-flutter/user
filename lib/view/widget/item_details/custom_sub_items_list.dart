import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/item_details_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';

class CustomSubItemsList extends GetView<ItemDetailsControllerImp> {
  const CustomSubItemsList({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        ...List.generate(
          controller.state.subItems.length,
          (index) => Container(
            margin: const EdgeInsets.only(right: 10),
            alignment: Alignment.center,
            height: 60,
            width: 90,
            decoration: BoxDecoration(
              color: controller.state.subItems[index]['active'] == '1'
                  ? AppColor.black
                  : Colors.white,
              border: Border.all(
                color: AppColor.black,
              ),
              borderRadius: BorderRadius.circular(10),
            ),
            child: Text(
              controller.state.subItems[index]['name'],
              style: TextStyle(
                color: controller.state.subItems[index]['active'] == '1'
                    ? Colors.white
                    : AppColor.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
