import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/item_details_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/core/constants/link_server.dart';

class TopPageItemDetails extends GetView<ItemDetailsControllerImp> {
  const TopPageItemDetails({super.key});

  @override
  Widget build(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          height: 200,
          decoration: const BoxDecoration(
            color: AppColor.primaryColor,
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(20),
            ),
          ),
        ),
        Positioned(
          top: 50,
          right: AppDimensions.width / 6,
          left: AppDimensions.width / 6,
          child: Hero(
            tag: '${controller.state.item.id}',
            child: CachedNetworkImage(
              imageUrl:
                  '${AppLinkServer.storage}/${controller.state.item.image}',
              height: 250,
              fit: BoxFit.fill,
            ),
          ),
        )
      ],
    );
  }
}
