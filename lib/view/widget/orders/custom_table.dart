import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/controller/orders/order_details_controller/controller.dart';
import 'package:nextrade/core/constants/color.dart';
import 'package:nextrade/core/functions/translate_database.dart';

class CustomTable extends GetView<OrderDetailsControllerImp> {
  const CustomTable({super.key});

  @override
  Widget build(BuildContext context) {
    return Table(
      children: [
        TableRow(
          children: [
            Text(
              'product'.tr,
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: AppColor.primaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 22,
              ),
            ),
            Text(
              'quantity'.tr,
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: AppColor.primaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 22,
              ),
            ),
            Text(
              'price'.tr,
              textAlign: TextAlign.center,
              style: const TextStyle(
                color: AppColor.primaryColor,
                fontWeight: FontWeight.bold,
                fontSize: 22,
              ),
            ),
          ],
        ),
        ...List.generate(
          controller.state.items.length,
          (index) => TableRow(
            children: [
              Text(
                translateDatabase(
                  controller.state.items[index].nameAr,
                  controller.state.items[index].nameEn,
                ),
                textAlign: TextAlign.center,
                style: const TextStyle(fontSize: 18),
              ),
              Text(
                '${controller.state.items[index].count}',
                textAlign: TextAlign.center,
                style: const TextStyle(fontSize: 18),
              ),
              Text(
                '${controller.state.items[index].price} \$',
                textAlign: TextAlign.center,
              ),
            ],
          ),
        ),
      ],
    );
  }
}
