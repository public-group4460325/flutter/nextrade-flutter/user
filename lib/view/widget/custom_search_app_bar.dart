import 'package:flutter/material.dart';
import 'package:nextrade/core/constants/color.dart';

class CustomSearchAppBar extends StatelessWidget {
  final String title;
  final void Function()? onNotificationsPressed;
  final void Function()? onSearchPressed;
  final void Function(String)? onChanged;
  final TextEditingController searchController;

  const CustomSearchAppBar({
    super.key,
    required this.title,
    required this.searchController,
    this.onNotificationsPressed,
    this.onSearchPressed,
    this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 10),
      child: Row(
        children: [
          Expanded(
            child: TextFormField(
              decoration: InputDecoration(
                prefixIcon: IconButton(
                  onPressed: onSearchPressed,
                  icon: const Icon(
                    Icons.search,
                    color: Colors.blueGrey,
                  ),
                ),
                hintText: title,
                hintStyle: const TextStyle(fontSize: 18),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
                filled: true,
                fillColor: AppColor.textFormColor,
              ),
              onChanged: onChanged,
              controller: searchController,
            ),
          ),
          const SizedBox(width: 10),
          IconButton(
            onPressed: onNotificationsPressed,
            icon: const Icon(
              Icons.notifications_active_outlined,
              size: 30,
              color: Colors.blueGrey,
            ),
          ),
        ],
      ),
    );
  }
}
