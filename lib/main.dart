import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:nextrade/core/constants/dimensions.dart';
import 'package:nextrade/core/localization/change_locale_controller/controller.dart';
import 'package:nextrade/core/localization/translation.dart';
import 'package:nextrade/core/services/my_services.dart';
import 'package:nextrade/initial_bindings.dart';
import 'package:nextrade/routes/routes.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initialServices();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    // FirebaseMessaging.instance.getToken().then((value) {
    //   print('===============');
    //   print(value);
    // });
    AppDimensions.init(context);
    ChangeLocaleControllerImp controller = Get.put(ChangeLocaleControllerImp());
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      translations: MyTranslation(),
      locale: controller.state.language,
      theme: controller.state.appTheme,
      initialBinding: InitialBindings(),
      getPages: routes,
    );
  }
}
