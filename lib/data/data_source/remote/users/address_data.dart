
import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class AddressData {
  CRUD crud;
  AddressData(this.crud);
  getData(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getAddresses,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  addData(
    String apiToken,
    String name,
    String city,
    String street,
    String phone,
    String lat,
    String long,
  ) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.addAddress,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {
        'name': name,
        'city': city,
        'street': street,
        'phone': phone,
        'location_lat': lat,
        'location_long': long,
      },
    );
    return response.fold((l) => l, (r) => r);
  }

  deleteData( int addressId,String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.deleteAddress,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {'address_id': '$addressId'},
    );
    return response.fold((l) => l, (r) => r);
  }

  updateData(
    String apiToken,
    int addressId,
    String name,
    String city,
    String street,
    String phone,
    String lat,
    String long,
  ) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.updateAddress,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {
        'address_id': '$addressId',
        'name': name,
        'city': city,
        'street': street,
        'phone': phone,
        'location_lat': lat,
        'location_long': long,
      },
    );
    return response.fold((l) => l, (r) => r);
  }
}
