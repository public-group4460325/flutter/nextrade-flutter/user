import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class ItemsData {
  CRUD crud;
  ItemsData(this.crud);
  getData(int categoryId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getItemsByCategory,
      body: {'category_id': '$categoryId'},
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }
}
