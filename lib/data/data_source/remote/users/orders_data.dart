import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class OrdersData {
  CRUD crud;
  OrdersData(this.crud);
  getPendingOrders(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getPendingOrders,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  getArchivedOrders(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getArchivedOrders,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  getOrderDetails(int orderId, String apiToken) async {
    var response = await crud.postData(
        linkUrl: AppLinkServer.getOrderDetails,
        headers: {'Authorization': 'Bearer $apiToken'},
        body: {'order_id': '$orderId'});
    return response.fold((l) => l, (r) => r);
  }

  deleteOrder(int orderId, String apiToken) async {
    var response = await crud.postData(
        linkUrl: AppLinkServer.deleteOrder,
        headers: {'Authorization': 'Bearer $apiToken'},
        body: {'order_id': '$orderId'});
    return response.fold((l) => l, (r) => r);
  }

  rateOrder(int orderId, double rate, String comment, String apiToken) async {
    var response = await crud.postData(
        linkUrl: AppLinkServer.rateOrder,
        headers: {'Authorization': 'Bearer $apiToken'},
        body: {'order_id': '$orderId', 'rate': '$rate', 'comment': comment});
    return response.fold((l) => l, (r) => r);
  }
}
