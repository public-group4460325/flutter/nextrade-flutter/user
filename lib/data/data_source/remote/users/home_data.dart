import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class HomeData {
  CRUD crud;
  HomeData(this.crud);
  getData(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getHomeData,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  searchData(String name, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.search,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {'name': name},
    );
    return response.fold((l) => l, (r) => r);
  }
}
