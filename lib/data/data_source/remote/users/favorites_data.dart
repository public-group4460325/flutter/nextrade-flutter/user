import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class FavoritesData {
  CRUD crud;
  FavoritesData(this.crud);
  getData(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getMyFavorites,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  addOrRemoveFavoritesData(int itemId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.addOrRemoveFromFavorites,
      body: {'item_id': '$itemId'},
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }
}
