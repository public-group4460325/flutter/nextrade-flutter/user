import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class CartData {
  CRUD crud;
  CartData(this.crud);
  addToCart(int itemId, int count, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.addToCart,
      body: {'item_id': '$itemId', 'count': '$count'},
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  removeFromCart(int itemId, int count, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.removeFromCart,
      body: {'item_id': '$itemId', 'count': '$count'},
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  getData(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getAllCartItems,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  updateCount(int itemId, int count, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.updateCount,
      body: {'item_id': '$itemId', 'count': '$count'},
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  deleteFromCart(int itemId, String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.deleteFromCart,
      body: {'item_id': '$itemId'},
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }

  checkCoupon(String apiToken, String code) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.checkCoupon,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {'code': code},
    );
    return response.fold((l) => l, (r) => r);
  }
}
