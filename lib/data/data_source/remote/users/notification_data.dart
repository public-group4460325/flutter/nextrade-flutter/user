import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class NotificationsData {
  CRUD crud;
  NotificationsData(this.crud);
  getData(String apiToken) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.getAllNotifications,
      headers: {'Authorization': 'Bearer $apiToken'},
    );
    return response.fold((l) => l, (r) => r);
  }
}
