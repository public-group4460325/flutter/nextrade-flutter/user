import 'package:nextrade/core/classes/crud.dart';
import 'package:nextrade/core/constants/link_server.dart';

class CheckOutData {
  CRUD crud;
  CheckOutData(this.crud);
  checkOut({
    int? addressId,
    required String receiveType,
    required String paymentType,
    required double shipping,
    required double price,
    int? couponId,
    required String apiToken,
  }) async {
    var response = await crud.postData(
      linkUrl: AppLinkServer.addOrder,
      headers: {'Authorization': 'Bearer $apiToken'},
      body: {
        'address_id': addressId != null ? '$addressId' : '',
        'receive_type': receiveType,
        'payment_type': paymentType,
        'shipping': '$shipping',
        'price': '$price',
        'coupon_id': couponId != null ? '$couponId' : '',
      },
    );
    return response.fold((l) => l, (r) => r);
  }
}
