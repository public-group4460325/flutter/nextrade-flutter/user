import '../../../../core/classes/crud.dart';
import '../../../../core/constants/link_server.dart';

class SignUpData {
  CRUD crud;
  SignUpData(this.crud);
  postData({
    required String name,
    required String password,
    required String email,
  }) async {
    var body = {
      'name': name,
      'password': password,
      'email': email,
    };
    var response =
        await crud.postData(linkUrl: AppLinkServer.signUp, body: body);
    return response.fold((l) => l, (r) => r);
  }
}
