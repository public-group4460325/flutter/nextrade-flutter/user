import '../../../../core/classes/crud.dart';
import '../../../../core/constants/link_server.dart';

class VerifyCodeSignUpData {
  CRUD crud;
  VerifyCodeSignUpData(this.crud);
  postData({
    required String verificationCode,
    required String email,
  }) async {
    var body = {
      'verification_code': verificationCode,
      'email': email,
    };
    var response = await crud.postData(
        linkUrl: AppLinkServer.verifyCodeSignUp, body: body);
    return response.fold((l) => l, (r) => r);
  }

  resendVerificationCode({required String email}) async {
    var body = {'email': email};
    var response = await crud.postData(
        linkUrl: AppLinkServer.resendVerificationCode, body: body);
    return response.fold((l) => l, (r) => r);
  }
}
