import '../../../../../core/classes/crud.dart';
import '../../../../../core/constants/link_server.dart';

class ResetPasswordData {
  CRUD crud;
  ResetPasswordData(this.crud);
  postData({
    required String email,
    required String password,
  }) async {
    var body = {
      'email': email,
      'password': password,
    };
    var response =
        await crud.postData(linkUrl: AppLinkServer.resetPassword, body: body);
    return response.fold((l) => l, (r) => r);
  }
}
