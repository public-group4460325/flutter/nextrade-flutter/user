import '../../../../../core/classes/crud.dart';
import '../../../../../core/constants/link_server.dart';

class VerifyCodeForgetPasswordData {
  CRUD crud;
  VerifyCodeForgetPasswordData(this.crud);
  postData({
    required String verificationCode,
    required String email,
  }) async {
    var body = {
      'email': email,
      'verification_code': verificationCode,
    };
    var response = await crud.postData(
        linkUrl: AppLinkServer.verifyCodeForgetPassword, body: body);
    return response.fold((l) => l, (r) => r);
  }

  resendVerificationCode({required String email}) async {
    var body = {'email': email};
    var response = await crud.postData(
        linkUrl: AppLinkServer.resendVerificationCode, body: body);
    return response.fold((l) => l, (r) => r);
  }
}
