import 'package:get/get.dart';

import '../../../core/constants/image_assets.dart';
import '../../models/onboarding_model.dart';

List<OnBoardingModel> onBoardingList = [
  OnBoardingModel(
      title: 'choose_product'.tr,
      body: 'choose_product_caption'.tr,
      imageUrl: AppImageAsset.onBoardingImageFast),
  OnBoardingModel(
      title: 'easy_safe_payment'.tr,
      body: 'easy_safe_payment_caption'.tr,
      imageUrl: AppImageAsset.onBoardingImageMap),
  OnBoardingModel(
      title: 'track_your_order'.tr,
      body: 'track_your_order_caption'.tr,
      imageUrl: AppImageAsset.onBoardingImageSafe),
  OnBoardingModel(
      title: 'fast_delivery'.tr,
      body: 'fast_delivery_caption'.tr,
      imageUrl: AppImageAsset.onBoardingImageSelect),
];
