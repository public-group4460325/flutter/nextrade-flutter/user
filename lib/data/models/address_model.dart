class Address {
  int? id;
  int? userId;
  String? name;
  String? city;
  String? street;
  String? phone;
  double? locationLat;
  double? locationLong;
  String? createdAt;
  String? updatedAt;

  Address({
    this.id,
    this.userId,
    this.name,
    this.city,
    this.street,
    this.phone,
    this.locationLat,
    this.locationLong,
    this.createdAt,
    this.updatedAt,
  });

  Address.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    name = json['name'];
    city = json['city'];
    street = json['street'];
    phone = json['phone'].toString();
    locationLat = json['location_lat'];
    locationLong = json['location_long'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['user_id'] = userId;
    data['name'] = name;
    data['city'] = city;
    data['street'] = street;
    data['phone'] = phone;
    data['location_lat'] = locationLat;
    data['location_long'] = locationLong;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
