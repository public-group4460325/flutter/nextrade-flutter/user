class RadioModel {
  bool isSelected;
  final int value;

  RadioModel({required this.isSelected, required this.value});
}