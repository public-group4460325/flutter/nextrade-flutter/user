class Coupon {
  int? id;
  String? code;
  int? count;
  String? expiredDate;
  int? discount;
  String? createdAt;
  String? updatedAt;

  Coupon(
      {this.id,
      this.code,
      this.count,
      this.expiredDate,
      this.discount,
      this.createdAt,
      this.updatedAt});

  Coupon.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    code = json['code'];
    count = json['count'];
    expiredDate = json['expired_date'];
    discount = json['discount'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['code'] = code;
    data['count'] = count;
    data['expired_date'] = expiredDate;
    data['discount'] = discount;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
