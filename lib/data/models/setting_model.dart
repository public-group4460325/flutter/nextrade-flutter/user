class Setting {
  int? id;
  String? titleAr;
  String? bodyAr;
  String? titleEn;
  String? bodyEn;
  String? createdAt;
  String? updatedAt;

  Setting(
      {this.id,
      this.titleAr,
      this.bodyAr,
      this.titleEn,
      this.bodyEn,
      this.createdAt,
      this.updatedAt});

  Setting.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    titleAr = json['title_ar'];
    bodyAr = json['body_ar'];
    titleEn = json['title_en'];
    bodyEn = json['body_en'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['title_ar'] = titleAr;
    data['body_ar'] = bodyAr;
    data['title_en'] = titleEn;
    data['body_en'] = bodyEn;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }
}
