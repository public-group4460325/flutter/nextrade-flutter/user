
import 'routes_links.dart';

List<GetPage<dynamic>> routes = [
  // Auth
  GetPage(
    name: AppRoute.login,
    page: () => const Login(),
    binding: LoginBindings(),
  ),
  GetPage(
    name: AppRoute.signUp,
    page: () => const SignUp(),
    binding: SignUpBindings(),
  ),
  GetPage(
    name: AppRoute.forgetPassword,
    page: () => const ForgetPassword(),
    binding: ForgetPasswordBindings(),
  ),
  GetPage(
    name: AppRoute.verifyCodeForgetPassword,
    page: () => const VerifyCodeForgetPassword(),
    binding: VerifyCodeForgetPasswordBindings(),
  ),
  GetPage(
    name: AppRoute.verifyCodeSignUp,
    page: () => const VerifyCodeSignUp(),
    binding: VerifyCodeSignUpBindings(),
  ),
  GetPage(
    name: AppRoute.resetPassword,
    page: () => const ResetPassword(),
    binding: ResetPasswordBindings(),
  ),
  GetPage(
    name: AppRoute.successSignUp,
    page: () => const SuccessSignUp(),
    binding: SuccessSignUpBindings(),
  ),
  GetPage(
    name: AppRoute.successResetPassword,
    page: () => const SuccessResetPassword(),
    binding: SuccessResetPasswordBindings(),
  ),
  // OnBoarding
  GetPage(
    name: AppRoute.onBoarding,
    page: () => const OnBoarding(),
    binding: OnBoardingBindings(),
  ),
  GetPage(
    name: '/',
    page: () => const Language(),
    binding: ChangeLanguageBindings(),
    middlewares: [MyMiddleware()],
  ),
  // Home
  GetPage(
    name: AppRoute.homeScreen,
    page: () => const HomeScreen(),
    binding: HomeScreenBindings(),
  ),
  GetPage(
    name: AppRoute.home,
    page: () => const Home(),
    binding: HomeBindings(),
  ),
  GetPage(
    name: AppRoute.items,
    page: () => const Items(),
    binding: ItemsBindings(),
  ),
  GetPage(
    name: AppRoute.itemDetails,
    page: () => const ItemDetails(),
    binding: ItemDetailsBindings(),
  ),
  GetPage(
    name: AppRoute.favorites,
    page: () => const Favorites(),
    binding: FavoritesBindings(),
  ),
  GetPage(
    name: AppRoute.cart,
    page: () => const Cart(),
    binding: CartBindings(),
  ),

  // Addresses
  GetPage(
    name: AppRoute.addressView,
    page: () => const AddressView(),
    binding: AddressViewBindings(),
  ),
  GetPage(
    name: AppRoute.addressAdd,
    page: () => const AddressAdd(),
    binding: AddressAddBindings(),
  ),
  GetPage(
    name: AppRoute.addressAddDetails,
    page: () => const AddressAddDetails(),
    binding: AddressAddDetailsBindings(),
  ),
  GetPage(
    name: AppRoute.checkOut,
    page: () => const CheckOut(),
    binding: CheckOutBindings(),
  ),
  GetPage(
    name: AppRoute.orders,
    page: () => const PendingOrders(),
    binding: PendingOrdersBindings(),
  ),
  GetPage(
    name: AppRoute.archivedOrders,
    page: () => const ArchivedOrders(),
    binding: ArchivedOrdersBindings(),
  ),
  GetPage(
    name: AppRoute.notifications,
    page: () => const Notifications(),
    binding: NotificationsBindings(),
  ),
  GetPage(
    name: AppRoute.ordersDetails,
    page: () => const OrderDetails(),
    binding: OrderDetailsBindings(),
  ),
  GetPage(
    name: AppRoute.tracking,
    page: () => const Tracking(),
    binding: TrackingBindings(),
  ),
];
